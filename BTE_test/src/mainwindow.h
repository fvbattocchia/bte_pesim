#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemModel>
#include <qregularexpression.h>
#include <QThread>


class ContenedorPlacas;
class InterpretePy;
class Interfazcfcc;
class PlacaWidget;
class Output;
class PaPDialog;
class ConexionesPaP;
class brokerConnetion;
class DialogIp;
class DialogTest;
class QTableWidget;
class QTableWidgetItem;
class QsciScintilla;
class CornerWidget;
class Simulacion;
//class QMap;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void guardarTabla(QString, int);
    void guardarPython(QString, int);
    bool controlTabla();
    void modificarEventos(QString, QTableWidget *, int, QsciScintilla *);
    int controlAsterisco(QString);
    void crearPlacas();
    bool mjsSave();
    void guardar(int);
    bool controlGuardar(QString,int);
    QByteArray openFile(QString);
    void crearArchivo(QString);
    bool controlPy();
    bool controlTarea();
    bool controlPap();
    void puertoEpp();
    void abrirPap();
    void abrirTareas();

    ~MainWindow();
private slots:
    void cellChanged(PlacaWidget*,int,int);
    void editorChanged();
    void eliminartodo();
    void on_actionStop_triggered();
    void on_actionPlay_triggered();
    void on_actionGuardar_triggered();
    void abrirProyecto(QString);
    void on_actionAbrir_Proyecto_triggered();
    void on_actionBorrar_triggered();
    void on_actionClonar_triggered();
    void on_actionArchivo_nuevo_triggered();
    void on_treeView_doubleClicked(const QModelIndex &index);
    void on_actionGuardar_todos_triggered();
    void on_Placa_tabCloseRequested(int index);
    void on_consola_returnPressed();
    void editorPap();
    void editorTareas();
    void savepap();
    void saveTareas();
    void limpiarEditorLog();
    void EditorLog(QString, QString);
    void EditorComp(QString, QString);
    void KillLog(QString, QString);
    void disableIconData();
    void on_lptPortEdit_returnPressed();

    void on_ip_port_tcpEdit_returnPressed();
    void conectarConsola(bool);
    void confIp();
    void saveip();
    void controlConec();

protected:
    virtual void resizeEvent(QResizeEvent *event);
private:
    Ui::MainWindow *ui;
    QFileSystemModel model;


    ContenedorPlacas *placas;
    InterpretePy *py;
    Interfazcfcc *interfLow;
    Output *salidaInfo;
    ConexionesPaP *conpap;

    CornerWidget *c;
    PaPDialog *DialogPap;
    DialogIp *dialogIp;
    DialogTest *dialogTest;
    QTimer * timer;

    QString namePap;
    QString pathPap;
    QString nameTareas;
    QString pathTareas;
    bool modif;
    QString dir;
    bool papOpen;
    bool tareaOpen;
    QString ip;

    brokerConnetion *conr;
    bool flagConr;
    QMap<QString,int>tareas;

    QThread thread;
    Simulacion* sim;
};


#endif // MAINWINDOW_H
