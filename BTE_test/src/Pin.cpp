/*
 * Pin.cpp
 *
 *  Created on: 24/7/2017
 *      Author: Battocchia Florencia
 */

#include "Pin.h"
#include "Registro.h"
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
Pin::Pin(bool e, int b, Registro *r,bool o,bool i) {
	invert= e;
	bit=b;
	registro= r;
    output=o;
    input=i;
}

int Pin::getValue(){
  if(invert)
      return !registro->getStateBit(bit);
  else
      return  registro->getStateBit(bit);


}

void Pin::setValue(int value){
	  if(invert){
		  if(value==0)
			  value=1;
		  else if(value==1)
              value=0;
	  }
      registro->setStateBit(bit,value);

}
bool Pin::getOut(){
    return output;
}
bool Pin::getInp(){
    return input;
}
Pin::~Pin() {

}
