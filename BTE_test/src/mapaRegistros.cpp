/*
 * mapaRegistros.cpp
 *
 *  Created on: 30 ago. 2017
 *      Author: Florencia
 */

#include "mapaRegistros.h"
#include "InterfazCom.h"
#include <string>
#include "InterpretePy.h"
#include "Output.h"
#include "ConexionesPaP.h"
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
using namespace std;
#define BOOL_STR(b) ((b)?"si":"no")
mapaRegistros::mapaRegistros(ConexionesPaP *c,string p, InterpretePy *interp) {
    con=c;
    placa=p;
    py=interp;
    clockOn=false;
    contador=0;
    periodo=0;
}
void mapaRegistros::crearRegName(string n,string onAccess,string onRead,string onWrite,string onExit){
    reg_n.access =onAccess;
    reg_n.read=onRead;
    reg_n.write=onWrite;
    reg_n.exit=onExit;
	registersname[n]=reg_n;
}
void mapaRegistros::crearRegAddress(uint64_t address,string n, int cant, string onAccess,string onRead,string onWrite,string onExit){
	reg_a.name=n;
	reg_a.offset=cant;
    reg_a.access =onAccess;
    reg_a.read=onRead;
    reg_a.write=onWrite;
    reg_a.exit=onExit;
	registeraddress[address]=reg_a;
}
uint64_t mapaRegistros::count(uint64_t address){
	return registeraddress.count(address);

}
uint64_t mapaRegistros::count (string name){
	return registersname.count(name);
}
bool mapaRegistros::isWrite(string name){
   return !(registersname.at(name).write.empty());
}
bool mapaRegistros::isWrite(uint64_t address){
    return !(registeraddress.at(address).write.empty());

}
bool mapaRegistros::isRead(string name){
    return !(registersname.at(name).read.empty());

}
bool mapaRegistros::isRead(uint64_t address){
    return !(registeraddress.at(address).read.empty());
}
void mapaRegistros::write(string name, uint64_t data,bool var){
    if(count(name)==1){
		if(isWrite(name)){
            py->write(placa,registersname.at(name).write,data,var, name);
            con->actualizar(name);
		}
		else
            Output::instance().setLogError(QString("El registro %1 no es de escritura").arg(name.c_str()).toStdString());
    }
	else
        Output::instance().setLogError(QString("El registro %1 no existe").arg(name.c_str()).toStdString());
}
void mapaRegistros::write(uint64_t address, uint64_t data, bool elog){
	bool var=false;
	if(count(address)==1){
		if(isWrite(address)){
            py->write(placa,registeraddress.at(address).write,address,data,elog,registeraddress.at(address).name);
            var=true;
		}
		else{
            Output::instance().setLogError(QString("La direccion %1 no pertenece a un registro de escritura").arg(address).toStdString());
		}
	}
	else{
        uint64_t max;
        uint64_t min;
		for (const auto& kv : registeraddress){
            min=kv.first;
            max=min+kv.second.offset;
			if(address>min and address<max){
                if(!kv.second.write.empty()){
                    py->write(placa,kv.second.write,address,data,elog,kv.second.name);
                    var=true;

				}
				else{
                    Output::instance().setLogError(QString("La direccion %1 no pertenece a un registro de escritura").arg(kv.first).toStdString());
				}
				break;
			}
		}
	}
	if(var)
        con->actualizar(to_string(address));
	else
        Output::instance().setLogError(QString("La direccion %1 no existe").arg(address).toStdString());
}
//------------------------------------------------------------------------
uint64_t mapaRegistros::read(string name,bool elog){
    uint64_t data=0;
    if(count(name)==1){
    	if(isRead(name))
            data=py->read(placa,registersname.at(name).read,name,elog);
    	else
            Output::instance().setLogError(QString("El registro %1 no es de lectura").arg(name.c_str()).toStdString());
    }
    else
        Output::instance().setLogError(QString("El registro %1 no existe").arg(name.c_str()).toStdString());
    return data;
}
uint64_t mapaRegistros::read(uint64_t address,bool elog){
    uint64_t data=0;
	bool var=true;
	if(count(address)==1){
		if(isRead(address)){
            data=py->read(placa,registeraddress.at(address).read,address,registeraddress.at(address).name,elog);
			var=false;
		}
		else{
            Output::instance().setLogError(QString("La direccion %1 no pertenece a un registro de lectura").arg(address).toStdString());
		}
	}
	else{
        uint64_t max;
        uint64_t min;
		for (auto& kv : registeraddress){
			min=kv.first;
            max=min+kv.second.offset;
			if(address>min and address<max){
                if(!kv.second.read.empty()){
                    data=py->read(placa,kv.second.read,address,kv.second.name,elog);
					var=false;
				}
				else{
                    Output::instance().setLogError(QString("La direccion %1 no pertenece a un registro de lectura").arg(address).toStdString());
                    var=false;
                }
				break;
			}
		}
	}
	if(var)
        Output::instance().setLogError(QString("La direccion %1 no existe").arg(address).toStdString());
    return data;
}
string mapaRegistros::info(){
	string retorno="";
	string name;
	int address;
	for (const auto& kv : registersname){
		name = kv.first;
        retorno= retorno +name + " " + "lectura " + BOOL_STR(isRead(name)) + " " + "escritura " +BOOL_STR(isWrite(name)) +"\n";
	}
	for (const auto& kv : registeraddress){
		address = kv.first;
		name =kv.second.name;
        retorno= retorno + name + " " + "lectura " + BOOL_STR(isRead(address)) + " " + "escritura "+ BOOL_STR(isWrite(address))+"\n";
	}
    return retorno;
}

void mapaRegistros::startClock(int p)
{
    periodo=p;
    clockOn=true;
    connect(&timer, SIGNAL(timeout()), this, SLOT(clockTarea()),Qt::QueuedConnection);
    timer.start(periodo);
}

void mapaRegistros::stopClock()
{
    clockOn=false;
    disconnect(&timer, SIGNAL(timeout()), this, SLOT(clockTarea()));
    timer.stop();
    contador=0;
}
mapaRegistros::~mapaRegistros() {
}

void mapaRegistros::clockTarea()
{
    contador++;
    py->ejecutarTarea(placa,contador,periodo);
}

