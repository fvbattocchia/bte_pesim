#ifndef SIMULACION_H
#define SIMULACION_H

#include <QObject>

class eppPc;
class eppPlaca;
class Signal;
class Interfazcfcc;
class Simulacion : public QObject
{
    Q_OBJECT
public:
    explicit Simulacion(QObject *parent = 0);
    void stopSim();
    int startSim(Interfazcfcc *);
    ~Simulacion();

signals:

public slots:
    void process();
    uint8_t server(uint8_t * );
private:

    eppPc *epp;
    eppPlaca *board_lpt;
    Signal *wait;
    bool playy;
};

#endif // SIMULACION_H
