/*
 * SimBoard.cpp
 *
 *  Created on: 25 jul. 2017
 *      Author: Battocchia Florencia
 */
#include "eppPlaca.h"
#include "PinPlaca.h"
#include "Signal.h"
#include "Interfazcfcc.h"
#include <pthread.h>
#include "list"
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
using namespace std;


eppPlaca::eppPlaca(Interfazcfcc * i) {
	interf =i;
	createLPT();
}
void eppPlaca::createLPT(){
	for(int i=2; i<10;i++){
        pin[i]=  new PinPlaca(0);
	}
    pin[11]=  new PinPlaca(0);     //senal wait
    pin[1]=  new PinPlaca(1);     //senal wrire
    pin[14]=  new PinPlaca(1);   //senal data strob
    pin[17]=  new PinPlaca(1); 	//senal address strob
}

PinPlaca *eppPlaca::getPin(int i)
{
    return pin.at(i);
}
void eppPlaca::initSignal(Signal *ws){
	waitSignal=ws;
}
uint8_t eppPlaca::getDataPin(){
    uint8_t data=0;
    for(int i=9;i>=2;i--){
		data=data|pin[i]->getValue();

        if(i!=2)
			data<<=1;
	}
    return  data;
}
int eppPlaca::getPinValue(int i){
    return pin[i]->getValue();
}
void eppPlaca::setDataPin(int i,int data){
     pin[i]->setValue(data);
}
void eppPlaca::setData(uint8_t data){
	int state;
	for(int i=2;i<10;i++){
		state=data&1;
		setDataPin(i,state);
		data>>=1;
	}
}

void eppPlaca::managerSignal(){
    list<uint64_t> dato;
	switch(estado){
		case(cero):
            if(getPinValue(17)==0){
            //write y address strob--ciclo escritura address
                if(getPinValue(1)==0 and getPinValue(17)==0){
                    estado=uno;
                    break;
                }
                else{
                    estado=cero;
                    break;
                }
            }
            else if(getPinValue(14)==0){
                //write y data strob--ciclo escritura dato
                if(getPinValue(1)==0 and getPinValue(14)==0){
                    estado=dos;
                    break;
                }
                //write y data strob--ciclo lectura dato
                else if(getPinValue(1)==1 and getPinValue(14)==0){
                    estado=tres;
                    break;
                }
                else{
                    estado=cero;
                    break;
                }
            }
            else{
                estado=cero;
                break;
            }
		case(tres):
             dato=interf->write(dato);
             if(dato.empty()){
                 interf->message("ERROR no hay respuesta");
                 break;
             }
             setData(dato.back());
             setDataPin(11,1);
             waitSignal->sendSignal(false);
             estado=seis;
			 break;
		case(dos):
             dato.push_back(getDataPin());
             interf->write(dato);
             setDataPin(11,1);
             waitSignal->sendSignal(false);
			 estado=seis;
			 break;
		case(uno):
             dato.push_back(getDataPin());
             interf->write(dato);
			 setDataPin(11,1);
             waitSignal->sendSignal(false);
			 estado=cinco;
			 break;
		case(cuatro):
             setDataPin(11,0);
             waitSignal->sendSignal(false);
			 estado=cero;
			 break;
		case(cinco):
             if(getPinValue(1)==1 and getPinValue(17)==1){
				 estado=cuatro;
				 break;
			 }
			 else{
				 estado=cinco;
				 break;
			 }
		case(seis):
             if(getPinValue(1)==1 and getPinValue(14)==1){
				 estado=cuatro;
				 break;
			 }
			 else{
				 estado=seis;
				 break;
			 }
	}
}

eppPlaca::~eppPlaca() {
    for(const auto&k:pin){
        delete k.second;
    }

}
