#include "mainwindow.h"
#include <QApplication>

#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
int showUi(int argc, char *argv[]){
    int ret;
    QApplication a(argc, argv);
    MainWindow w;
   // w.showMaximized();
    w.show();
    ret = a.exec();
    return ret;
}

int main(int argc, char *argv[])
{
    int ret = showUi(argc,argv);
    if(ret==0){
        #ifdef LEAK_CHECK
            PrintMemoryLeakInfo();
        #endif
    }

    return ret;
}
