/*
 * Registro.cpp
 *
 *  Created on: 24/7/2017
 *      Author: Battocchia Florencia
 */
#include <iostream>
using namespace std;
#include "Registro.h"
#include "Pin.h"
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
Registro::Registro(bool r, bool w, int a) {
	read=r;
	write= w;
	address=a;
}

void  Registro::writeRegister(uint8_t data){
	int state;
	for(int i=0;i<8;i++){
		state=data&1;
        setStateBit(i,state);
		data>>=1;
	}
}

uint8_t Registro::readRegister( ){
    uint8_t data=0;
	for(int i=7;i>=0;i--){
        data=data|getStateBit(i);

		if(i!=0)
			data<<=1;
	}
    return data;
}

void Registro::crearPin(int n, bool inv, int b, Registro *r,bool o,bool i)
{
    pines[n]= new Pin(inv, b,r,o,i);


}

void Registro::crearBit(int i, int e)
{
    bits[i]= e;
}

Pin *Registro::getPin(int i)
{
    return pines.at(i);
}

void Registro::setStateBit(int i, int e)
{
    bits[i]=e;
}

int Registro::getStateBit(int i)
{
    return bits[i];
}
Registro::~Registro() {
    for(const auto&k:pines){
        delete k.second;
    }

}
