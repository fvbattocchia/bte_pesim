#ifndef BROKERCONNECTION_H
#define BROKERCONNECTION_H
#include <QObject>
#include "qmqtt.h"

class brokerConnetion: public QObject
{
    Q_OBJECT
public:
    brokerConnetion();
    void conectar(QString);
    void desconectar();
    bool  isConnected();
    void publicar(QString);
private slots:
    void connected();
    void received(const QMQTT::Message&);
    void error(QMQTT::ClientError);

private:
     QMQTT::Client *client;
};

#endif // BROKERCONNECTION_H
