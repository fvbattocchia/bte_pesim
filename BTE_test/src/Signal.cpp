/*
 * Signal.cpp
 *
 *  Created on: 23/7/2017
 *      Author: Battocchia Florencia
 */

#include "Signal.h"
#include "Pin.h"
#include "PinPlaca.h"
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
#define low 0
#define high 1
Signal::Signal(Pin* p,PinPlaca* pb) {
	pin_LPT=p;
	state = pin_LPT->getValue();
	pin_board=pb;
}

void Signal::sendSignal(bool enable){
    if((pin_LPT->getOut()and !pin_LPT->getInp()) || (pin_LPT->getInp()and pin_LPT->getOut() and enable)){
		state=pin_LPT->getValue();
		pin_board->setValue(state);
	}
    else if((pin_LPT->getInp()and !pin_LPT->getOut())||(pin_LPT->getInp()and pin_LPT->getOut() and !enable)){
		state=pin_board->getValue();
		pin_LPT->setValue(state);
	}
    else
        return;
}

Signal::~Signal() {

}
