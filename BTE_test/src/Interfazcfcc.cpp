/*
 * Interfazcfcc.cpp
 *
 *  Created on: 28 ago. 2017
 *      Author: Battocchia Florencia
 */

#include "Interfazcfcc.h"
#include "ContenedorPlacas.h"
#include "InterpretePy.h"
#include "Output.h"
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
Interfazcfcc::Interfazcfcc(InterpretePy *p) {
    py=p;
    names=ContenedorPlacas::instance().getName();
}

list<uint64_t> Interfazcfcc::write(list<uint64_t> dato){
list<uint64_t> provisorio;
list<uint64_t> pack;
int colision=0;
    for(const auto& i:names){
        provisorio=py->capaDeCom(i,dato);
        if(!provisorio.empty()){
            pack=provisorio;
            colision++;
        }
    }
    if(colision>1)
        Output::instance().setLogError("Colision de datos en las respues");
    return pack;
}

void Interfazcfcc::message(string mjs)
{
    Output::instance().kill(mjs);
}
Interfazcfcc::~Interfazcfcc() {

}

