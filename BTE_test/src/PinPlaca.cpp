/*
 * pinBoard.cpp
 *
 *  Created on: 25 jul. 2017
 *      Author: Battocchia Florencia
 */

#include "PinPlaca.h"
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
PinPlaca::PinPlaca(int v) {
	setValue(v);

}

void PinPlaca::setValue(int v){
	value= v;
}

PinPlaca::~PinPlaca() {

}
