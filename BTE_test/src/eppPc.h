/*
 * PuertoParaleo.h
 *
 *  Created on: 25 ago. 2017
 *      Author: Battocchia Florencia
 */

#ifndef EPPPC_H_
#define EPPPC_H_
#include <windows.h>
#include <iostream>
#include <pthread.h>
#include <map>
using namespace std;

#include<inpout32.h>
class Pin;
class Registro;
class Signal;
class eppPlaca;
class ManagerTimer;



class eppPc {
public:
    eppPc();
	void crear();
    Signal* conectar(eppPlaca*);
    uint8_t read_register(uint16_t);
    void write_register(uint16_t, uint8_t);
	void dataWriteCycle();
	void addressWriteCycle();
	void dataReadCycle();
	void addressReadCycle();
    virtual ~eppPc();
private:
	//********************registros********************************
	Registro *data_Port;
	Registro *statusPort;
	Registro *controlPort;
	Registro *addressPort;
	Registro *dataPort;
	//***************Signal*********************************//
    Signal* data[8];
	Signal* wait;
	Signal* write;
	Signal* dataStrob;
	Signal* addressStrob;
	//***********************PLACA******************************//
    eppPlaca* board;

};

#endif /* EPPPC_H_ */
