#include "placawidget.h"
#include "ui_placawidget.h"
#include <Qsci/qscilexerpython.h>
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
PlacaWidget::PlacaWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PlacaWidget)
{

    ui->setupUi(this);
    ui->tabla->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    editor = ui->editorTex;
    crearEditor();
    connect(this,SIGNAL(cellChanged(PlacaWidget*,int,int)),parent,SLOT(cellChanged(PlacaWidget*,int,int)));
    connect(this,SIGNAL(modificationChanged()),parent, SLOT(editorChanged()));

}
void PlacaWidget::crearEditor(){


// codes based from http://eli.thegreenplace.net/2011/04/01/sample-using-qscintilla-with-pyqt/
   //initializeFont
   QFont font("consolas", 10);
   font.setFixedPitch(true);
   editor->setFont(font);
   //initializeMargin (margen donde van los numeros, cambia el margen cdo cambia el numero de linea)
   QFontMetrics fontmetrics = QFontMetrics(editor->font());
   editor->setMarginsFont(editor->font());
   editor->setMarginWidth(0, fontmetrics.width(QString::number(editor->lines()))+2);
   editor->setMarginLineNumbers(0, true);
   editor->setMarginsBackgroundColor(QColor("#cccccc"));
   //initializeCaretLine
   // Current line visible with special background color
   editor->setCaretLineVisible(true);
   editor->setCaretLineBackgroundColor(QColor("#F2F0F0"));
   //initializeLexer (analizar lexico del lenguaje, y autocompletado)
   QsciLexerPython *lexer = new QsciLexerPython(this);
   lexer->setFont(font);
   lexer->setDefaultFont(font);
   lexer->setFoldComments(true); //achicar comentarios
   lexer->setFoldCompact(false);
   editor->setLexer(lexer);
   // code based on QSciTE https://code.google.com/p/qscite/
   //initializeFolding (margen 1, plegado de codigo)
   editor->setFolding(QsciScintilla::BoxedTreeFoldStyle);
   // editor->setText();
   editor->setIndentationGuides(true);
   editor->setIndentationsUseTabs(false);
   editor->setTabWidth(2);
   editor->setAutoIndent(true);
}
PlacaWidget::~PlacaWidget()
{
    delete ui;
}


//para el autocompletado de las casillas 1 3 4 5 6 y buscar en el editor
void PlacaWidget::on_tabla_cellClicked(int row, int column)
{
    QString name;
    QString address;
    QString text;
    QTableWidgetItem *itab;
    itab = ui->tabla->item(row,0);
    address=itab->text();
    itab = ui->tabla->item(row,2);
    name=itab->text();
    itab = ui->tabla->item(row,column);
    text=itab->text();
    if(column==1 and !address.isEmpty() and text.isEmpty())
        itab->setText("1");
    if(!name.isEmpty()and text.isEmpty()){
        switch (column){
        case 5:
            text=QString("On%1Write").arg(name);
            itab->setText(text);
            itab->setToolTip(text);
            if(!address.isEmpty())
                editor->append("def " +text+"(address, dato):\n\tpass\n\n");
            else
                editor->append("def " +text+"(dato):\n\tpass\n\n");
            editor->SendScintilla(QsciScintilla::SCI_GOTOPOS, editor->length());
            break;
        case 4:
            text=QString("On%1Read").arg(name);
            itab->setText(text);
            itab->setToolTip(text);
            if(!address.isEmpty()){
                editor->append("def " +text+"(address):\n\treturn 0\n\n");
            }
            else{
                editor->append("def " +text+"():\n\treturn 0\n\n");
            }
            editor->SendScintilla(QsciScintilla::SCI_GOTOPOS, editor->length());
            break;
        case 6:
            text=QString("On%1Exit").arg(name);
            itab->setText(text);
            itab->setToolTip(text);
            editor->append("def "+text+"(none):\n\tpass\n\n");
            editor->SendScintilla(QsciScintilla::SCI_GOTOPOS, editor->length());
            break;
        case 3:
            text=QString("On%1Access").arg(name);
            itab->setText(text);
            itab->setToolTip(text);
            editor->append("def "+text+"(none):\n\tpass\n\n");
            editor->SendScintilla(QsciScintilla::SCI_GOTOPOS, editor->length());
            break;
        default:
            break;
        }
    }
    else if(!name.isEmpty()and !text.isEmpty()){
        editor->findFirst(text,false,true,true,true);
    }
}


void PlacaWidget::on_tabla_cellChanged(int row, int column)
{
    emit cellChanged(this,row,column);
}

void PlacaWidget::on_editorTex_textChanged()
{
    QFontMetrics fontmetrics = QFontMetrics(editor->font());
    editor->setMarginWidth(0, fontmetrics.width(QString::number(editor->lines()))+2);
    emit modificationChanged();
}
