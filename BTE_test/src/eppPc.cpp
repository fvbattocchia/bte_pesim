/*
 * PuertoParaleo.cpp
 *
 *  Created on: 25 ago. 2017
 *      Author: Battocchia Florencia
 */

#include "eppPc.h"
#include "eppPlaca.h"
#include "Registro.h"
#include "Signal.h"
#include "ManagerTimer.h"
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
#define data_PortAddress  0x378+0
#define addressPortAddress 0x378+3
#define dataPortAddress 0x378+4
#define controlPortAddress 0x378+2
#define statusPortAddress 0x378+1
#define bit0 0
#define bit1 1
#define bit2 2
#define bit3 3
#define bit4 4
#define bit5 5
#define bit6 6
#define bit7 7
#define pin1 1
#define pin10 10
#define pin11 11
#define pin12 12
#define pin13 13
#define pin14 14
#define pin15 15
#define pin16 16
#define pin17 17
//***********************************************************
#define time_out 1000
eppPc::eppPc() {
	crear();
}
void eppPc::crear(){
//********************registros********************************
    data_Port= new Registro(true, true,data_PortAddress); // read, write, address
    statusPort= new Registro(true, false, statusPortAddress);
    controlPort= new Registro(true, true, controlPortAddress);
    addressPort= new Registro(true, true, addressPortAddress);
    dataPort= new Registro(true, true, dataPortAddress);
//*********************REGISTRO DATA PORT **************************************//
    int j=0;
    int bit;
    int pin;
	for (int i=0; i<8;i++)
        data_Port->crearBit(i,0);
    for (int i=2; i<10;i++){
        bit =j;
        pin=i;
        data_Port->crearPin(pin,false,bit,data_Port,true,true);
        j++;
    }
//****************** REGISTRO STATUS PORT ********************************//
    for (int i=0; i<8;i++){
        if(i==3||i==7)
            statusPort->crearBit(i,1);
        else
            statusPort->crearBit(i,0);
    }
    statusPort->crearPin(pin15,false,bit3,statusPort,false,true);
    statusPort->crearPin(pin13,false,bit4,statusPort,false,true);
    statusPort->crearPin(pin12,false,bit5,statusPort,false,true);
    statusPort->crearPin(pin10,false,bit6,statusPort,false,true);
    statusPort->crearPin(pin11,true,bit7,statusPort,false,true);
//****************** REGISTRO CONTROL PORT ********************************//
    for (int i=0; i<8;i++){
        if(i==2)
            controlPort->crearBit(i,1);
        else
            controlPort->crearBit(i,0);
    }
    controlPort->crearPin(pin1,true,bit0,controlPort,true,false);
    controlPort->crearPin(pin14,true,bit1,controlPort,true,false);
    controlPort->crearPin(pin16,true,bit2,controlPort,true,false);
    controlPort->crearPin(pin17,true,bit3,controlPort,true,false);
//*****************REGISTRO ADDRESS PORT ESPECIAL***************************//
	for (int i=0; i<8;i++)
        addressPort->crearBit(i,0);
//****************REGISTRO DATA PORT ESPECIAL***************************//
	for (int i=0; i<8;i++)
        dataPort->crearBit(i,0);
}

Signal* eppPc::conectar(eppPlaca *b){
	board=b;
    int j=2;
    for(int i=0;i<8;i++){
        data[i]=new Signal(data_Port->getPin(j),board->getPin(j));
        j++;
    }
    wait = new Signal(statusPort->getPin(11),board->getPin(11));
    write = new Signal(controlPort->getPin(1),board->getPin(1));
    dataStrob = new Signal(controlPort->getPin(14),board->getPin(14));
    addressStrob = new Signal(controlPort->getPin(17),board->getPin(17));
	return wait;
}
void eppPc::write_register(uint16_t PortAddress,uint8_t data){
	switch(PortAddress){
	case data_PortAddress:
		if(data_Port->getWrite())
			data_Port->writeRegister(data);
		break;
	case addressPortAddress:
		if(addressPort->getWrite()){
			addressPort->writeRegister(data);
			addressWriteCycle();
		}
		break;
	case dataPortAddress:
		if(dataPort->getWrite()){
			dataPort->writeRegister(data);
			dataWriteCycle();
		}
		break;
	case controlPortAddress:
		if(controlPort->getWrite())
			controlPort->writeRegister(data);
		break;
	case statusPortAddress:
		if(statusPort->getWrite())
			statusPort->writeRegister(data);
		break;
	default:
		break;
	}
}
uint8_t eppPc::read_register(uint16_t PortAddress ){
    uint8_t data;
	switch(PortAddress){
	case data_PortAddress:
		if(data_Port->getRead())
			data=data_Port->readRegister();
		break;
	case addressPortAddress:
		if(addressPort->getRead()){
			addressReadCycle();
			data=addressPort->readRegister();
		}
		break;
	case dataPortAddress:
		if(dataPort->getRead()){
			dataReadCycle();
			data=dataPort->readRegister();
		}
		break;
	case controlPortAddress:
		if(controlPort->getRead())
			data=controlPort->readRegister();
		break;
	case statusPortAddress:
		if(statusPort->getRead())
			data=statusPort->readRegister();
		break;
	default:
		break;
	}
	return data;
}
void eppPc::dataWriteCycle(){

    controlPort->setStateBit(0,1);
    write->sendSignal(true);
    for(int i=0;i<8;i++){
        data_Port->setStateBit(i,dataPort->getStateBit(i));
    }

    for(int i=0;i<8;i++){
        data[i]->sendSignal(true);
    }

    while(wait->getState()==HIGH){
        Sleep(0);
	}
    controlPort->setStateBit(1,1);
    dataStrob->sendSignal(true);

    while(wait->getState()==LOW){
        Sleep(0);
	}
    controlPort->setStateBit(1,0);
    dataStrob->sendSignal(true);

    controlPort->setStateBit(0,0);
    write->sendSignal(true);

    while(wait->getState()==HIGH){
        Sleep(0);
	}

}
void eppPc::addressWriteCycle(){

    controlPort->setStateBit(0,1);
    write->sendSignal(true);
	for(int i=0;i<8;i++)
        data_Port->setStateBit(i,addressPort->getStateBit(i));

    for(int i=0;i<8;i++){
        data[i]->sendSignal(true);
    }
    while(wait->getState()==HIGH){
          Sleep(0);
	}
    controlPort->setStateBit(3,1);
    addressStrob->sendSignal(true);

    while(wait->getState()==LOW){
          Sleep(0);
	}
    controlPort->setStateBit(3,0);
    addressStrob->sendSignal(true);

    controlPort->setStateBit(0,0);
    write->sendSignal(true);

    while(wait->getState()==HIGH){
        Sleep(0);
	}

}
void eppPc::dataReadCycle(){

    controlPort->setStateBit(0,0);
    write->sendSignal(true);

    while(wait->getState()==HIGH){
        Sleep(0);
	}
    controlPort->setStateBit(1,1);
    dataStrob->sendSignal(true);

    while(wait->getState()==LOW){
        Sleep(0);
    }

	//datos van al puerto paralelo
    for(int i=0;i<8;i++){
        data[i]->sendSignal(false);
    }
	//los datos se copian en data port
	for(int i=0;i<8;i++){
        dataPort->setStateBit(i,data_Port->getStateBit(i));
	}
    controlPort->setStateBit(1,0);
    dataStrob->sendSignal(true);


    while(wait->getState()==HIGH){
        Sleep(0);
	}

}
void eppPc::addressReadCycle(){

    controlPort->setStateBit(0,0);
    write->sendSignal(true);

    while(wait->getState()==HIGH){
            Sleep(0);
	}

    controlPort->setStateBit(3,1);
    addressStrob->sendSignal(true);

    while(wait->getState()==LOW){
                Sleep(0);
	}
	//datos van al puerto paralelo
    for(int i=0;i<8;i++){
        data[i]->sendSignal(false);
    }
	//los datos se copian en data port
	for(int i=0;i<8;i++)
        addressPort->setStateBit(i,data_Port->getStateBit(i));

    controlPort->setStateBit(3,0);
    addressStrob->sendSignal(true);

    while(wait->getState()==HIGH){
            Sleep(0);
	}

}
eppPc::~eppPc() {


    delete write;
    delete dataStrob;
    delete addressStrob;

    for(int i=0;i<8;i++){
        delete data[i];
    }

    delete data_Port;

    delete statusPort;

    delete controlPort;

    delete addressPort;

    delete dataPort;


}

