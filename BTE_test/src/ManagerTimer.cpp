/*
 * ManagerTimer.cpp
 *
 *  Created on: 28 ago. 2017
 *      Author: Battocchia Florencia
 */

#include "ManagerTimer.h"
#include <sched.h>
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
ManagerTimer::ManagerTimer() {
	tiempo=0;

}
void ManagerTimer::tick(){
	tiempo++;
    if(tiempo>100000)
        tiempo=0;
}
void ManagerTimer::delay(int t){
	int start = tiempo;
	int end= start+t;

	while(tiempo<= end){
		sched_yield();
	}
}
bool ManagerTimer::isTimeLess(int end){
	if(tiempo<end)
		return true;
	else
		return false;
}

ManagerTimer::~ManagerTimer() {

}

