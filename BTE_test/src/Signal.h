/*
 * Signal.h
 *
 *  Created on: 23/7/2017
 *      Author: Battocchia Florencia
 */

#ifndef SIGNAL_H_
#define SIGNAL_H_

#include <iostream>
using namespace std;

class Pin;
class PinPlaca;

class Signal{
public:
    Signal(Pin*, PinPlaca *);
	int getState() {return state;}
	void sendData();
	void reciveData();
    void sendSignal(bool enable);
	virtual ~Signal();
private:
	int state;
	Pin* pin_LPT;
    PinPlaca* pin_board;
};

#endif /* SIGNAL_H_ */
