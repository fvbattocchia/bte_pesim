/*
 * InterfazComandos.h
 *
 *  Created on: 28 ago. 2017
 *      Author: Battocchia Florencia
 */

#ifndef INTERFAZCOM_H_
#define INTERFAZCOM_H_
#include <sstream>
#include <string>
#include <iostream>
using namespace std;
class QMainWindow;
class InterfazCom {
public:

    static InterfazCom& instance();
    void getMw(QMainWindow *);
    void enableComanboard(bool);
    virtual ~InterfazCom();
    string ejecutaComando(string);
private:
    InterfazCom();
    //static InterfazCom* con;
    bool comb;
    QMainWindow *obj;
};

#endif /* InterfazCom */
