/*
 * testLPT.cpp
 *
 *  Created on: 17 jul. 2017
 *      Author: Battocchia Florencia
 */
#include<inpout32.h>
#include <iostream>
#include <Simulacion.h>
using namespace std;
/***************************************************/
Simulacion *sim;

void configRecive(Simulacion *var){
    sim=var;
}

uint8_t server_cb(uint8_t *Buffer){
    uint8_t retVal;
    //Qt::DirectConnection
    //Qt::BlockingQueuedConnection
    //Qt::QueuedConnection

    QMetaObject::invokeMethod(sim,"server",Qt::DirectConnection,  Q_RETURN_ARG(uint8_t, retVal), Q_ARG(uint8_t *, Buffer));
    return retVal;
}
void stopCallback(){
    stopWorker();
}

void startCallback(){
    setcallback(server_cb);
    startWorker();
}

int startPuerto(){
    int portOpen=0;
    if(IsInpOutDriverOpen())
        portOpen= 1;
    else
        portOpen= 0;

    return portOpen;
}

