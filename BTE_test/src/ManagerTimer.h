/*
 * ManagerTimer.h
 *
 *  Created on: 28 ago. 2017
 *      Author: Battocchia Florencia
 */

#ifndef MANAGERTIMER_H_
#define MANAGERTIMER_H_

class ManagerTimer {
public:
	ManagerTimer();
	void tick();
	void delay(int);
	bool isTimeLess(int);
    int getTime() {return tiempo;}
	virtual ~ManagerTimer();
private:
	int tiempo;
};

#endif /* MANAGERTIMER_H_ */
