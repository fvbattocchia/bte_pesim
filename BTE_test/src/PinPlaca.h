/*
 * pinBoard.h
 *
 *  Created on: 25 jul. 2017
 *      Author: Battocchia Florencia
 */

#ifndef PINPLACA_H_
#define PINPLACA_H_

class PinPlaca {
public:
    PinPlaca(int);
    int  getValue(){return value;}
	void setValue(int);
    virtual ~PinPlaca();
private:
	int value;
};

#endif /* PINPLACA_H_ */
