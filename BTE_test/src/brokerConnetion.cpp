#include "brokerConnetion.h"
#include "InterfazCom.h"
#include "Output.h"
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
brokerConnetion::brokerConnetion()
{

}

void brokerConnetion::connected()
{
     client->subscribe("comando",0);
     //client->subscribe("placa",0);
}

void brokerConnetion::received(const QMQTT::Message & message)
{
    InterfazCom::instance().ejecutaComando(QString(message.payload()).toStdString());
    //qDebug()<<message.payload();
}

void brokerConnetion::error(QMQTT::ClientError e)
{

    switch (e) {
        case QMQTT::ClientError::UnknownError:
            Output::instance().setError("UnknownError");
            break;
        case  QMQTT::ClientError::SocketConnectionRefusedError:
            Output::instance().setError("SocketConnectionRefusedError");
            break;
        case QMQTT::ClientError::SocketRemoteHostClosedError:
            Output::instance().setError("SocketRemoteHostClosedError");
            break;
        case QMQTT::ClientError::SocketHostNotFoundError:
            Output::instance().setError("SocketHostNotFoundError");
            break;
        case QMQTT::ClientError::SocketAccessError:
            Output::instance().setError("SocketAccessError");
            break;
        case QMQTT::ClientError::SocketResourceError:
            Output::instance().setError("SocketResourceError");
            break;
        case QMQTT::ClientError::SocketTimeoutError:
            Output::instance().setError("SocketTimeoutError");
            break;
        case QMQTT::ClientError::SocketNetworkError:
            Output::instance().setError("SocketNetworkError");
            break;
        case QMQTT::ClientError::SocketAddressInUseError:
            Output::instance().setError("SocketAddressInUseError");
            break;
        case QMQTT::ClientError::SocketAddressNotAvailableError:
            Output::instance().setError("SocketAddressNotAvailableError");
            break;
        case QMQTT::ClientError::SocketUnsupportedSocketOperationError:
            Output::instance().setError("SocketUnsupportedSocketOperationError");
            break;
        case QMQTT::ClientError::SocketUnfinishedSocketOperationError:
            Output::instance().setError("SocketUnfinishedSocketOperationError");
            break;
        case QMQTT::ClientError::SocketProxyAuthenticationRequiredError:
            Output::instance().setError("SocketProxyAuthenticationRequiredError");
            break;
        case QMQTT::ClientError::SocketSslHandshakeFailedError:
            Output::instance().setError("SocketSslHandshakeFailedError");
            break;
        case QMQTT::ClientError::SocketProxyConnectionRefusedError:
            Output::instance().setError("SocketProxyConnectionRefusedError");
            break;
        case QMQTT::ClientError::SocketProxyConnectionClosedError:
            Output::instance().setError("SocketProxyConnectionClosedError");
            break;
        case QMQTT::ClientError::SocketProxyConnectionTimeoutError:
            Output::instance().setError("SocketProxyConnectionTimeoutError");
            break;
        case QMQTT::ClientError::SocketProxyNotFoundError:
            Output::instance().setError("SocketProxyNotFoundError");
            break;
        case QMQTT::ClientError::SocketOperationError:
            Output::instance().setError("SocketOperationError");
            break;
        case QMQTT::ClientError::SocketSslInternalError:
            Output::instance().setError("SocketSslInternalError");
            break;
        case QMQTT::ClientError::SocketSslInvalidUserDataError:
            Output::instance().setError("SocketSslInvalidUserDataError");
            break;
       default:
            break;
    }
}

void brokerConnetion::conectar(QString var)
{
    QHostAddress hostaddress =QHostAddress(var);

    client = new QMQTT::Client(hostaddress, 1883);
    //error
    connect(client,SIGNAL(error(QMQTT::ClientError)),this,SLOT(error(QMQTT::ClientError)));
    //lo conecta
    client->connectToHost();
    //espera a que se conecte para subscribirse
    connect(client,SIGNAL(connected()),this,SLOT(connected()));
    //mensaje
    connect(client,SIGNAL(received(const QMQTT::Message&)),this,SLOT(received(const QMQTT::Message&)));

}

void brokerConnetion::desconectar()
{
    client->disconnectFromHost();
}

bool brokerConnetion::isConnected()
{
    return client->isConnectedToHost();
}

void brokerConnetion::publicar(QString mjs)
{
    client->publish(QMQTT::Message(0,"log",mjs.toLocal8Bit()));
}
