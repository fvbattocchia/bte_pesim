/*
 * ContenedorPlacas.cpp
 *
 *  Created on: 4 sep. 2017
 *      Author: Florencia
 */

#include "ContenedorPlacas.h"
#include "mapaRegistros.h"
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
//ContenedorPlacas *ContenedorPlacas::mboars=nullptr;

ContenedorPlacas::ContenedorPlacas() {


}
ContenedorPlacas &ContenedorPlacas::instance(){
    static ContenedorPlacas mboars;


    return mboars;
}
//------------------------------------------------------------------------------
void ContenedorPlacas::crearBoard(string name,ConexionesPaP *conpap,string namer, InterpretePy *py,string t){
	board b;
    b.ri=new mapaRegistros(conpap,namer,py);
    //b.ri=r;
	b.texto=t;
	boards[name]=b;
}

mapaRegistros* ContenedorPlacas::getRegister(string name){
	return boards[name].ri;
}
string ContenedorPlacas::getText(string name){
	return boards[name].texto;
}
int ContenedorPlacas::count (string name){
    return boards.count(name);
}

list<string> ContenedorPlacas::getName()
{
    list <string>names; 
    for (const auto& kv : boards)
        names.push_back(kv.first);
    return names;
    
}

void ContenedorPlacas::clearBoars()
{
    for (const auto& kv : boards){
        if(kv.second.ri->isClockOn())
            kv.second.ri->stopClock();
    }
    for (const auto& kv : boards){
          delete kv.second.ri;
    }

    boards.clear();
}
ContenedorPlacas::~ContenedorPlacas() {

}

