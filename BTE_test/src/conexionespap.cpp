#include "conexionespap.h"
#include "InterfazCom.h"
#include "Output.h"
#include <QString>
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
ConexionesPaP::ConexionesPaP(list<string> c_r,list<string>c_b)
{
    conex_reg=c_r;
    conex_bit=c_b;
    flag=true;
}
ConexionesPaP::ConexionesPaP(){

}

void ConexionesPaP::actualizar(string reg){
    if(flag){
        flag=false;
        string placa1;
        string placa2;
        string salida;
        string entrada;
        string bit1;
        string bit2;
        for(const auto& i:conex_bit){
            QString com=QString(i.c_str());
            QStringList coml=com.split(",");
            placa1=coml.at(0).trimmed().toStdString();
            salida=coml.at(1).trimmed().toStdString();
            bit1=coml.at(2).trimmed().toStdString();
            placa2=coml.at(3).trimmed().toStdString();
            entrada=coml.at(4).trimmed().toStdString();
            bit2=coml.at(5).trimmed().toStdString();
            if(salida==reg)
                conexion_bit(placa1,salida,bit1,placa2,entrada,bit2);
        }
        for(const auto& i:conex_reg){
            QString com=QString(i.c_str());
            QStringList coml=com.split(",");
            placa1=coml.at(0).trimmed().toStdString();
            salida=coml.at(1).trimmed().toStdString();
            placa2=coml.at(3).trimmed().toStdString();
            entrada=coml.at(4).trimmed().toStdString();
           if(salida==reg)
                 conexion(placa1,salida,placa2,entrada);

        }
        flag=true;
    }
}

void ConexionesPaP::conexion(string placa1, string salida,string placa2, string entrada){
    string retorno;
    //lectura
    salida= placa1+" " + salida;
    retorno=InterfazCom::instance().ejecutaComando(salida);
    if(!retorno.empty()){
        //escritura
        entrada= placa2+" " + entrada +" "+retorno;
        InterfazCom::instance().ejecutaComando(entrada);
    }
    else{
        string log="error en conexion entre placas:"+ placa1 +","+placa2+ "registros" +salida+","+ entrada;
        Output::instance().setLogError(log);
    }
}

void ConexionesPaP::conexion_bit(string placa1, string registro1, string bit1, string placa2, string registro2, string bit2)
{
    string valor;
    int eBit;
    string lectura;
    string escritura;
    lectura= placa1+" " + registro1;
    valor=InterfazCom::instance().ejecutaComando(lectura);
    if(!valor.empty()){
        eBit=(stoi(valor,nullptr,0)>>(stoi(bit1)))&1;
        lectura= placa2+" " + registro2;
        valor=InterfazCom::instance().ejecutaComando(lectura);
        if(!valor.empty()){
            int v=stoi(valor,nullptr,0);
            if(eBit==0){
                v=v&(~(1<<stoi(bit2)));
            }
            else if(eBit==1){
                v=v|(1<<stoi(bit2));
            }

            //escritura
            escritura= placa2+" " + registro2 +" "+to_string(v);
            InterfazCom::instance().ejecutaComando(escritura);
        }
        else{
            string log="error en conexion entre placas:"+ placa1 +","+placa2+ "registros" +registro1+","+ registro2;
            Output::instance().setLogError(log);
        }
    }
    else{
        string log="error en conexion entre placas:"+ placa1 +","+placa2+ "registros" +registro1+","+ registro2;
        Output::instance().setLogError(log);
    }

}
