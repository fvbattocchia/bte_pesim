/*
 * InterfazCom.cpp
 *
 *  Created on: 28 ago. 2017
 *      Author: Battocchia Florencia
 */

#include "InterfazCom.h"
#include "ContenedorPlacas.h"
#include "mapaRegistros.h"
#include "Output.h"
#include <QMainWindow>
#include <QMetaObject>
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
//InterfazCom *InterfazCom::con=nullptr;
InterfazCom::InterfazCom(){

}
InterfazCom &InterfazCom::instance(){
    static InterfazCom con;
    //si es nulo devuelve false
    /*
    if(nullptr == con){
        con=new InterfazCom();
    }
    */
    return con;
}

void InterfazCom::getMw(QMainWindow * mw)
{
    obj=mw;
    comb=false;
}

void InterfazCom::enableComanboard(bool cb)
{
    comb=cb;
}
//FUNCIONES PARA OBTER BOOL PARA EL CONTROL DE LAS PLACAS,
//
//*****************************************************************//
string InterfazCom::ejecutaComando(string linea){
    string retorno= "ok";
    bool var=true;
    bool var1=false;
    string placa;
    string comando;
	string dato;
	stringstream ss(linea);
	ss >> placa;
	ss >> comando;
	ss >> dato;
    if(placa=="play")
       QMetaObject::invokeMethod(obj,"on_actionPlay_triggered",Qt::QueuedConnection);
    else if(placa=="stop")
       QMetaObject::invokeMethod(obj,"on_actionStop_triggered",Qt::QueuedConnection);
    else if(placa=="abrir"){
        if(!comando.empty()){
            QString path=QString(comando.c_str());
            QMetaObject::invokeMethod(obj,"abrirProyecto",Qt::QueuedConnection, Q_ARG(QString, path));
        }
    }
    else if(placa=="limpiar")
        QMetaObject::invokeMethod(obj,"limpiarEditorLog",Qt::QueuedConnection);
    else if(placa=="mjs"){
        if(!comando.empty())
            Output::instance().setLog("MENSAJE: "+comando);
    }
    else if(placa=="ayuda"){
        string ayuda="";
        ayuda=ayuda +
"play: comienza la simulacion"+"\n" +
"stop: para la simulacion"+"\n" +
"limpiar: limpia la consola"+"\n" +
"tag <comando>: tag en log"+"\n" +
"---comandos validos solo durante la simulacion------"+"\n" +
"<placa> <direccion>:  escribe en registro con direccion"+"\n" +
"<placa> <nombre>: lee en registro con nombre"+"\n" +
"<placa> <direccion> <dato> :escribe en registro con direccion"+"\n" +
"<placa> <nombre> <dato> :escribe en registro con nombre"+"\n" +
"<placa> placa: informacion de los registros de la placa";

        Output::instance().logInfo(ayuda);
    }
    else{
        if(ContenedorPlacas::instance().count(placa)==1 && !comando.empty() && comb){
            if(comando=="placa"){
                Output::instance().logInfo( ContenedorPlacas::instance().getRegister(placa)->info());
            }
            else{
                int address;
                try {
                    address=stoi(comando,nullptr,0);
                }
                catch (const std::invalid_argument&) {
                    var=false;
                }
                //escritura de registros por nombre o direccion
                if (!dato.empty()){
                    if(!var)
                        ContenedorPlacas::instance().getRegister(placa)->write(comando, stoi(dato,nullptr,0),false);
                    else
                        ContenedorPlacas::instance().getRegister(placa)->write(address, stoi(dato,nullptr,0),false);
                }
                //lectura de registros por nombre o direccion
                else{
                    if(!var)
                       retorno=to_string(ContenedorPlacas::instance().getRegister(placa)->read(comando,false));
                    else
                       retorno=to_string(ContenedorPlacas::instance().getRegister(placa)->read(address,false));
               }
           }
        }
        else{
            var1=true;
        }
      }
      if(var1)
         Output::instance().setLogError("comando no valido");
      return retorno;
}

InterfazCom::~InterfazCom() {

}

