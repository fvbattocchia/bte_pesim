/*
 * Pin.h
 *
 *  Created on: 24/7/2017
 *      Author: Battocchia Florencia
 */

#ifndef PIN_H_
#define PIN_H_


class Registro;

class Pin {
public:
    Pin(bool, int, Registro* , bool o, bool i);
	bool getInvert() {return invert;}
	int  getValue();
	void setValue(int v);
	bool getOut();
	bool getInp();
	virtual ~Pin();
private:
	bool invert;
    int bit;
	Registro* registro;
    bool input;
    bool output;
};

#endif /* PIN_H_ */
