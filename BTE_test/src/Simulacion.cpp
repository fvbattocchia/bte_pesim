#include "simulacion.h"
#include <windows.h>
#include <iostream>
#include <pthread.h>

#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
using namespace std;
class Interfazcfcc;
#include "Signal.h"
#include "eppPlaca.h"
#include "eppPc.h"

void stopCallback();
void startCallback();
int startPuerto();
void configRecive(Simulacion *);
Simulacion::Simulacion(QObject *parent) : QObject(parent)
{
    configRecive(this);
}

uint8_t Simulacion::server(uint8_t * Buffer){
    uint16_t PortAddress;
    uint16_t* pBuffer;
    pBuffer = (uint16_t *)&Buffer[1];
    PortAddress= *pBuffer;
    uint8_t data = 0;

    switch(Buffer[0]){
        case READ:
            data= epp->read_register(PortAddress);
            break;
        case WRITE:
            epp->write_register(PortAddress,Buffer[3]);
            break;
    }

    return data;
}

Simulacion::~Simulacion()
{


}
void Simulacion::stopSim(){
    stopCallback();
    playy=false;

    delete board_lpt;
    delete epp;
    delete wait;

}
void Simulacion::process(){
    while(playy){
        board_lpt->managerSignal();
        Sleep(0);
    }
}
int Simulacion::startSim(Interfazcfcc *interfLow){
    playy=true;
    //capa fisica
    board_lpt=new eppPlaca(interfLow);
    epp= new eppPc();
    wait=epp->conectar(board_lpt);
    board_lpt->initSignal(wait);
    startCallback();
    return startPuerto();
}
