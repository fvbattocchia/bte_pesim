#ifndef PAPDIALOG_H
#define PAPDIALOG_H

#include <QDialog>

namespace Ui {
class PaPDialog;
}

class PaPDialog : public QDialog{
    Q_OBJECT

public:
    explicit PaPDialog(QWidget *parent = 0);
    ~PaPDialog();
    void removeRows(const uint32_t from = 0, const uint32_t to = 0);

private slots:
    void on_savePap_clicked();
    void on_eliminarFila_clicked();
    void on_agregarFila_clicked();
signals:
    void savepap();
private:
    Ui::PaPDialog *ui;
};

#endif // PAPDIALOG_H
