/*
 * ContenedorPlacas.h
 *
 *  Created on: 4 sep. 2017
 *      Author: Florencia
 */

#ifndef CONTENEDORPLACAS_H_
#define CONTENEDORPLACAS_H_
#include <iostream>
#include <map>
#include <string>
#include <list>
using namespace std;
class mapaRegistros;
class ConexionesPaP;
class InterpretePy;
class ContenedorPlacas {
public:
    static ContenedorPlacas &instance();
    void crearBoard(string, ConexionesPaP *, string, InterpretePy *, string);
    mapaRegistros* getRegister(string);
	string getText(string);
	int count (string);
    list<string> getName();
    void clearBoars();



private:
    ContenedorPlacas();
    virtual ~ContenedorPlacas();

	struct board{
            mapaRegistros *ri;
			string texto;
		};
	map<string,board>boards;
};

#endif /* CONTENEDORPLACAS_H_ */
