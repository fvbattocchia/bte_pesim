/*
 * SimBoard.h
 *
 *  Created on: 25 jul. 2017
 *      Author: Battocchia Florencia
 */

#ifndef EPPPLACA
#define EPPPLACA

#include <iostream>
#include <map>

using namespace std;


class PinPlaca;
class Signal;
class Interfazcfcc;

class eppPlaca {
public:

    eppPlaca(Interfazcfcc *);
	void createLPT();
    PinPlaca* getPin(int);
    uint8_t getDataPin();
    int getPinValue(int);
    void setDataPin(int, int);
    void setData(uint8_t);
    void initSignal(Signal*);
    void managerSignal();
    virtual ~eppPlaca();

private:
    map<int,PinPlaca*> pin;
	Signal* waitSignal;
	Interfazcfcc *interf;
	enum eEstados {
		cero,
		uno,
		dos,
		tres,
		cuatro,
		cinco,
		seis,
	 };
	eEstados estado=cuatro;
};

#endif /* EPPPLACA_H_ */
