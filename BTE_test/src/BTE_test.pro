#-------------------------------------------------
#
# Project created by QtCreator 2017-09-03T13:47:10
#
#-------------------------------------------------
QT       += core gui concurrent svg network

CONFIG += c++11
#CONFIG += leakCheck

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BTE_test
TEMPLATE = app

ENCODING = UTF-8

#soporte de pthread
QMAKE_CXXFLAGS += -pthread

INCLUDEPATH += ../../Librerias/inpout32 \
               ../../Librerias/Qtmqtt\include\
               ../../Librerias/nanomsg\include \
               ../../Librerias/python/include \
               ../../Librerias/QScintilla/include \
               ../../Librerias/cppMemDbg

SOURCES += main.cpp\
        mainwindow.cpp \
    Interfazcfcc.cpp \
    ManagerTimer.cpp \
    Pin.cpp \
    Registro.cpp \
    Signal.cpp \
    placawidget.cpp \
    cornerwidget.cpp \
    interpretepy.cpp \
    Output.cpp \
    conexionespap.cpp \
    dialogip.cpp \
    brokerConnetion.cpp \
    eppPlaca.cpp \
    eppPc.cpp \
    InterfazCom.cpp \
    mapaRegistros.cpp \
    PinPlaca.cpp \
    ContenedorPlacas.cpp \
    dialogtest.cpp \
    PaPDialog.cpp \
    Simulacion.cpp \
    controlLib.cpp

HEADERS  += mainwindow.h \
    Interfazcfcc.h \
    ManagerTimer.h \
    Pin.h \
    Registro.h \
    Signal.h \
    placawidget.h \
    cornerwidget.h \
    interpretepy.h \
    Output.h \
    conexionespap.h \
    dialogip.h \
    brokerConnetion.h \
    eppPlaca.h \
    eppPc.h \
    InterfazCom.h \
    mapaRegistros.h \
    PinPlaca.h \
    ContenedorPlacas.h \
    dialogtest.h \
    PaPDialog.h \
    Simulacion.h

#memoria

leakCheck{
    DEFINES += LEAK_CHECK
    SOURCES += ../../Librerias/cppMemDbg\\cppMemDbg.cpp
    HEADERS += ../../Librerias/cppMemDbg\\cppMemDbg.h
}

FORMS += mainwindow.ui \
        placawidget.ui \
        cornerwidget.ui \
        dialogip.ui \
        dialogtest.ui \
        PaPDialog.ui

RESOURCES += \
    resources.qrc

LIBS += -L..\\..\\Librerias\\inpout32\\Release -linpout32
#qtmqtt
LIBS += -L..\\..\\Librerias\\Qtmqtt\\lib -lQtmqtt
#python
LIBS += -L..\\..\\Librerias\\python\\libs -lpython35
#nanomsg
LIBS += -L..\\..\\Librerias\\nanomsg\\lib -lnanomsg
#QScintilla
LIBS += -L..\\..\\Librerias\\QScintilla\\lib -lqscintilla2_qt5
LIBS += -pthread

RC_FILE = icono.rc


