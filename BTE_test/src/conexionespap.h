#ifndef CONEXIONESPAP_H
#define CONEXIONESPAP_H
#include <sstream>
#include <string>
#include <iostream>
#include "list"
using namespace std;
class ConexionesPaP
{
public:
    ConexionesPaP(list<string>, list<string>);
    ConexionesPaP();
    void actualizar(string);
    void conexion(string, string,string, string );
    void conexion_bit(string , string, string,string, string, string);
private:
    list<string>conex_reg;
    list<string>conex_bit;
    bool flag;
};

#endif // CONEXIONESPAP_H
