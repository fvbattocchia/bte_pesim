#include "dialogip.h"
#include "ui_dialogip.h"
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
DialogIp::DialogIp(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogIp)
{
    ui->setupUi(this);

}

DialogIp::~DialogIp()
{
    delete ui;
}

void DialogIp::on_toolButton_clicked()
{
    emit saveip();
}
