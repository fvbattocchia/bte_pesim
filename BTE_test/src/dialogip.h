#ifndef DIALOGIP_H
#define DIALOGIP_H

#include <QDialog>

namespace Ui {
class DialogIp;
}

class DialogIp : public QDialog
{
    Q_OBJECT

public:
    explicit DialogIp(QWidget *parent = 0);
    ~DialogIp();

private slots:
    void on_toolButton_clicked();
signals:
    void saveip();

private:
    Ui::DialogIp *ui;
};

#endif // DIALOGIP_H
