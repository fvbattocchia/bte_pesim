/*
 * testLPT.cpp
 *
 *  Created on: 17 jul. 2017
 *      Author: Battocchia Florencia
 */
#include <windows.h>
#include <iostream>
#include <pthread.h>
#include <map>
#include <qmqtt.h>
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
using namespace std;
#include<inpout32.h>
#include "Signal.h"
#include "eppPlaca.h"
#include "eppPc.h"

class Interfazcfcc;
eppPc *epp;
eppPlaca *board_lpt;
Signal *wait;
volatile bool playy;
pthread_t tiids;
/********************************************************/
uint8_t server_cb(uint8_t *Buffer){
    uint16_t PortAddress;
    uint16_t* pBuffer;
    pBuffer = (uint16_t *)&Buffer[1];
	PortAddress= *pBuffer;
    uint8_t data = 0;

	switch(Buffer[0]){
		case READ:
            data= epp->read_register(PortAddress);
            break;
		case WRITE:
            epp->write_register(PortAddress,Buffer[3]);
            break;
	}
	return data;
}

void *updatePlugin(void *arg) {
    int i= SetThreadPriority(GetCurrentThread(),THREAD_PRIORITY_LOWEST);
    cout<<"prioridad "<<i<<endl;
    cout<< GetThreadPriority(GetCurrentThread())<<endl;
    while(playy){
        board_lpt->managerSignal();
        Sleep(0);
    }
    return arg;
}

void stopPlugin(){
    //para la thread de la libreria
    stopWorker();
    playy=false;
    pthread_join(tiids, NULL);

    delete board_lpt;
    delete epp;
    delete wait;
}
int startPlugin(Interfazcfcc *interfLow){
	cout << "!!!Start sim LPT!!!" << endl;
     cout<< "main thread: "<<GetThreadPriority(GetCurrentThread())<<endl;
    int t2;
    playy=true;
    //capa fisica
    board_lpt=new eppPlaca(interfLow);
    epp= new eppPc();
	wait=epp->conectar(board_lpt);
	board_lpt->initSignal(wait);

    t2=pthread_create(&tiids, NULL, updatePlugin, NULL);

    setcallback(server_cb);
    //inicializa la thread de la libreria
    startWorker();
    int portOpen=0;
    if(!IsInpOutDriverOpen()&&t2>0)
        portOpen= 0;
    else
        portOpen= 1;

    return portOpen;
}

