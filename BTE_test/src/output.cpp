#include "Output.h"
#include <QTextEdit>
#include <QDateTime>
#include <QMessageBox>
#include <QTextStream>
#include <QDebug>
#include <QMainWindow>
#include <QMetaObject>
#include <QList>
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif

//----------------------------------------------------------------------------------
Output::Output(){}
//----------------------------------------------------------------------------------
Output &Output::instance(){
    //si es nulo devuelve false

    static Output out;
    return out;
}

void Output::getEditor(QMainWindow *mobj)
{
    obj=mobj;
}
QString Output::remplazar(QString str)
{
    str.replace(">","&gt;");
    str.replace("<","&lt;");
    str.replace("\n","<br>");
    str.replace("\t", "&nbsp;");
    str.replace(" ", "&nbsp;");
    return str;

}
//log despues de play
void Output::setLog(string text)
{
 //   cout<<text<<endl;
    QDateTime dateTime = dateTime.currentDateTime();
    QString dateTimeString = dateTime.toString("dd-MM-yyyy hh:mm:ss");
    QString str = QString("%2 %1").arg(text.c_str()).arg(dateTimeString);
    saveLog(str);
    str=remplazar(str);
     QMetaObject::invokeMethod(obj,"EditorLog",Qt::QueuedConnection, Q_ARG(QString, str), Q_ARG(QString, "rgb(23,21,21)"));
}

void Output::logInfo(string text)
{
    QString str = QString(text.c_str());
    str=remplazar(str);
    QMetaObject::invokeMethod(obj,"EditorLog",Qt::QueuedConnection, Q_ARG(QString, str), Q_ARG(QString, "rgb(23,21,21)"));
}

void Output::kill(string text)
{
    cout<<text<<endl;
    QDateTime dateTime = dateTime.currentDateTime();
    QString dateTimeString = dateTime.toString("dd-MM-yyyy hh:mm:ss");
    QString str = QString("%2 %1").arg(text.c_str()).arg(dateTimeString);
    QMetaObject::invokeMethod(obj,"KillLog",Qt::QueuedConnection, Q_ARG(QString, str), Q_ARG(QString, "rgb(255,0,0)"));
    saveLog(str);
}

void Output::setLogError(string text)
{
    QDateTime dateTime = dateTime.currentDateTime();
    QString dateTimeString = dateTime.toString("dd-MM-yyyy hh:mm:ss");
    QString str = QString("%2 %1").arg(text.c_str()).arg(dateTimeString);
    QMetaObject::invokeMethod(obj,"EditorLog",Qt::QueuedConnection, Q_ARG(QString, str), Q_ARG(QString, "rgb(255,0,0)"));
    saveLog(str);
}
//log antes de la ejecucion
void Output::setWarning(string text)
{
    QString str = QString(text.c_str());
    QMetaObject::invokeMethod(obj,"EditorComp",Qt::QueuedConnection, Q_ARG(QString, str), Q_ARG(QString, "rgb(204,204,0)"));
}

void Output::setError(string text)
{
    QString str = QString(text.c_str());
    QMetaObject::invokeMethod(obj,"EditorComp",Qt::QueuedConnection, Q_ARG(QString, str), Q_ARG(QString, "rgb(255,0,0)"));
}

void Output::saveLog(QString text)
{
    QTextStream ts( &file );
    ts<<text<<"\r\n";
    ts.flush();
}
void Output::createFileLog(QString dir)
{
    QDateTime dateTime = dateTime.currentDateTime();
    QString dateTimeFecha = dateTime.toString("dd-MM");
    QString dateTimeHora = dateTime.toString("hh-mm");
    QString name=QString("%1\\log_%2-%3%4").arg(dir).arg(dateTimeFecha).arg(dateTimeHora).arg(".txt");
    file.setFileName(name);
    if (!file.open(QIODevice::Append)) {
        QMessageBox::information(nullptr,"Unable to open file",
            file.errorString());
        return;
    }

}


