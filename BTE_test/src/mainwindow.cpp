#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qtconcurrentrun.h>
#include <QInputDialog>
#include <QMenu>
#include <QMessageBox>
#include <QFileDialog>
#include <QTreeWidgetItem>
#include <QTableWidget>
#include <Qsci/qsciscintilla.h>
#include <QDebug>
#include <QTimer>
#include <QToolButton>
#include "list"
#include <QMap>
//***************
#include <PaPDialog.h>
#include "cornerwidget.h"
#include "placawidget.h"
#include "mapaRegistros.h"
#include "InterfazCom.h"
#include "ContenedorPlacas.h"
#include "interpretepy.h"
#include "interfazcfcc.h"
#include "Output.h"
#include "ConexionesPaP.h"
#include "brokerConnetion.h"
#include "DialogIp.h"
#include <DialogTest.h>
#include "Simulacion.h"

#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
using namespace QtConcurrent;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
   ui->setupUi(this);
   c=new CornerWidget(ui->Placa);
   ui->Placa->setCornerWidget(c,Qt::TopRightCorner);
  // ui->Placa->tabBar()->resize(c->sizeHint());
   DialogPap= new PaPDialog(this);
   dialogIp=new DialogIp(this);
   dialogTest=new DialogTest(this);

   ip="127.0.0.1";
   QLineEdit* editor;
   editor=dialogIp->findChild<QLineEdit*>("lineEdit");
   editor->insert(ip);
   connect(dialogIp,SIGNAL(saveip()),this,SLOT(saveip()));

   connect(c,SIGNAL(eliminartodo()),this,SLOT(eliminartodo()));
   connect(DialogPap,SIGNAL(savepap()),this,SLOT(savepap()));
   connect(dialogTest,SIGNAL(saveTareas()),this,SLOT(saveTareas()));
   connect(c,SIGNAL(filaeliminada()),this,SLOT(editorChanged()));
   //menu del toolbar boton herramientas
   QMenu *menu=new QMenu(this);
   menu->addAction("conexiones pap",this,SLOT(editorPap()));
   menu->addAction("Tareas",this,SLOT(editorTareas()));
   menu->addAction("Configurar IP",this,SLOT(confIp()));
   QAction *check;
   check=menu->addAction("Conexion remota",this,SLOT(conectarConsola(bool)));
   check->setCheckable(true);
   ui->actionHerramientas->setMenu(menu);
   QToolButton* herram=(QToolButton*)ui->mainToolBar->widgetForAction(ui->actionHerramientas);
   herram->setPopupMode(QToolButton::InstantPopup);

   //model = new QFileSystemModel(this);
   model.setReadOnly(false);
   //**************label*********************
   ui->conex->setStyleSheet("background-image: url(:/imagenes/network-disconnect.svg)");
   ui->tranfDatos->setStyleSheet("background-image: url(:/imagenes/link-inactive.svg)");
   //botones que empizan deshabilitados
   ui->actionPlay->setEnabled(false);
   ui->actionClonar->setEnabled(false);
   ui->actionBorrar->setEnabled(false);
   ui->actionGuardar->setEnabled(false);
   ui->actionGuardar_todos->setEnabled(false);
   ui->actionStop->setEnabled(false);
   papOpen=true;
   tareaOpen=true;

   Output::instance().getEditor(this);
   puertoEpp();
   InterfazCom::instance().getMw(this);

   flagConr=false;
   conr=new brokerConnetion();
}
MainWindow::~MainWindow()
{

    delete conr;
    delete DialogPap;
    delete dialogIp;
    delete dialogTest;
    delete c;
    delete ui;
}
void MainWindow::modificarEventos(QString text,QTableWidget *tabla,int row,QsciScintilla * editor){
    QString tant;
    QString tnuevo;

    if(!tabla->item(row,3)->text().isEmpty( )){
        tant=tabla->item(row,3)->text();
        tnuevo=QString("On%1Access").arg(text);
        tabla->item(row,3)->setText(tnuevo);
        editor->findFirst(tant,false,true,true,true);
        editor->replace(tnuevo);
    }
    if(!tabla->item(row,4)->text().isEmpty( )){
        tant=tabla->item(row,4)->text();
        tnuevo=QString("On%1Read").arg(text);
        tabla->item(row,4)->setText(tnuevo);
        editor->findFirst(tant,false,true,true,true);
        editor->replace(tnuevo);
    }
    if(!tabla->item(row,5)->text().isEmpty( )){
        tant=tabla->item(row,5)->text();
        tnuevo=QString("On%1Write").arg(text);
        tabla->item(row,5)->setText(tnuevo);
        editor->findFirst(tant,false,true,true,true);
        editor->replace(tnuevo);

    }
    if(!tabla->item(row,6)->text().isEmpty( )){
        tant=tabla->item(row,6)->text();
        tnuevo=QString("On%1Exit").arg(text);
        tabla->item(row,6)->setText(tnuevo);
        editor->findFirst(tant,false,true,true,true);
        editor->replace(tnuevo);
    }
}

//cambia de color el casillero de la tabla que tenga error y pone asterisco
void MainWindow::cellChanged(PlacaWidget * p, int row, int column)
{
    if(modif){
        editorChanged();
        QTableWidgetItem *item;
        QTableWidget * tabla;
        QString text;
        QsciScintilla *editor;

        QRegularExpression re;
        QRegularExpressionValidator v(re,0);
        int pos = 0;
        int state;
        editor=p->findChild<QsciScintilla *>("editorTex");
        tabla=p->findChild<QTableWidget *>("tabla");
        item=tabla->item(row,column);
        text=item->text();

        switch (column){
        case 0:
            re.setPattern("((?:0x)[0-9a-fA-F]+|[0-9]+)?");
            v.setRegularExpression(re);
            state=v.validate(text, pos);
            if(state==2)
                item->setBackground(Qt::white);
            else
                item->setBackground(QColor(255,121,121,255));
            //***********control tamaño******************************
            if(!tabla->item(row,0)->text().isEmpty()&& tabla->item(row,1)->text().isEmpty())
                tabla->item(row,1)->setBackground(QColor(255,121,121,255));
            //*************************************************************+
            break;
        case 1:
            re.setPattern("([0-9]*)");
            v.setRegularExpression(re);
            state=v.validate(text, pos);
            if(state==2)
                item->setBackground(Qt::white);
            else
                item->setBackground(QColor(255,121,121,255));
            break;
        case 2:
            re.setPattern("([a-zA-Z]+[0-9a-zA-Z_]*)");
            v.setRegularExpression(re);
            state=v.validate(text, pos);
            if(state==2){
                item->setBackground(Qt::white);
                modificarEventos(text,tabla,row,editor);
            }
            else
                item->setBackground(QColor(255,121,121,255));

            break;
        case 3:
           re.setPattern("(On[_a-zA-Z0-9]+Access)?");
           v.setRegularExpression(re);
           state=v.validate(text, pos);
           if(state==2)
               item->setBackground(Qt::white);
           else
               item->setBackground(QColor(255,121,121,255));
           break;
        case 4:
           re.setPattern("(On[_a-zA-Z0-9]+Read)?");
           v.setRegularExpression(re);
           state=v.validate(text, pos);
           if(state==2)
               item->setBackground(Qt::white);
           else
               item->setBackground(QColor(255,121,121,255));
           break;
        case 5:
           re.setPattern("(On[_a-zA-Z0-9]+Write)?");
           v.setRegularExpression(re);
           state=v.validate(text, pos);
           if(state==2)
               item->setBackground(Qt::white);
           else
               item->setBackground(QColor(255,121,121,255));
           break;
        case 6:
           re.setPattern("(On[_a-zA-Z0-9]+Exit)?");
           v.setRegularExpression(re);
           state=v.validate(text, pos);
           if(state==2)
               item->setBackground(Qt::white);
           else
               item->setBackground(QColor(255,121,121,255));
           break;
           default:
                break;
        }
    }
}
//************************************************************
//pone asterisco al cambiar el editor
void MainWindow::editorChanged()
{
    if(modif){
        QRegularExpression re("([a-z]+[0-9A-Za-z]*)");
        QRegularExpressionValidator v(re, 0);
        QString name;
        int pos=0;
        int state;
        int currentIndexTab = ui->Placa->currentIndex();
        name=ui->Placa->tabText(currentIndexTab);
        state=v.validate(name, pos);
        if(state==2){
            name=name+"*";
            ui->Placa->setTabText(currentIndexTab,name);
        }
    }
}
//*************************************************
void MainWindow::on_actionStop_triggered()
{
    if(!dir.isNull()){
        sim->stopSim();
        thread.quit();
        thread.wait();

        delete py;
        delete conpap;
        delete interfLow;
        ContenedorPlacas::instance().clearBoars();
        ui->Placa->setEnabled(true);
        ui->actionStop->setEnabled(false);
        ui->actionPlay->setEnabled(true);
        ui->conexion->setEnabled(true);
        ui->conex->setStyleSheet("background-image: url(:/imagenes/network-disconnect.svg)");
        DialogPap->findChild<QTableWidget*>("PaPTabla")->setEnabled(true);
        InterfazCom::instance().enableComanboard(false);
        Output::instance().setLog("Stop");
    }
    else
        Output::instance().setWarning("proyecto no especificado");
}
//*********************************************************
void MainWindow::on_actionPlay_triggered()
{
    bool comenzar=false;
    int openCom;
    if(!dir.isNull()){
        bool tablaOk;
        bool pyOk;
        bool tareasOk;

        on_actionGuardar_todos_triggered();
        py = new InterpretePy();
        tareasOk=controlTarea();
        tablaOk=controlTabla();
        pyOk=controlPy();
       if(QFileInfo(QDir(dir).absoluteFilePath(pathPap)).exists()){
            bool papOk;
            papOk=controlPap();
            if(tablaOk&&pyOk&&papOk&&tareasOk){
                comenzar=true;
            }
        }
        else{
             if(tablaOk&&pyOk&&tareasOk){
                 conpap= new ConexionesPaP();
                 comenzar=true;
             }
        }
        if(ui->ip_port_tcpEdit->text().isEmpty()){
           comenzar=false;
           Output::instance().setError("ip:portLPT no especificado");
        }
       }
       else
           Output::instance().setWarning("proyecto no especificado");

   if(comenzar){
       crearPlacas();

       sim = new Simulacion();
       sim->moveToThread(&thread);
       connect(&thread,SIGNAL(started()),sim,SLOT(process()));
       connect(&thread, SIGNAL (finished()),sim, SLOT (deleteLater()));
       openCom=sim->startSim(interfLow);
       thread.start();
       ui->Placa->setEnabled(false);
       ui->actionStop->setEnabled(true);
       ui->actionPlay->setEnabled(false);
       Output::instance().setLog("Play");
       ui->conex->setStyleSheet("background-image: url(:/imagenes/network-connect.svg)");

       ui->conexion->setEnabled(false);
       DialogPap->findChild<QTableWidget*>("PaPTabla")->setEnabled(false);
       InterfazCom::instance().enableComanboard(true);
       if(openCom==1)
           Output::instance().setLog("Comunicacion inicializada");
       else
           Output::instance().setLogError("Comunicacion no inicializada");
    }
}
//*********************************
void MainWindow::crearPlacas()
{
   QString name;
   QStringList filters;
   filters << "*.csv";
   QFileInfoList names = QDir(dir).entryInfoList(filters);
   for(const auto& k:names){
       name=k.baseName();
       if(name!=namePap && name!=nameTareas){
           //****************creo el contenedor de placas*****
          // ContenedorPlacas::instance()->crearBoard(name.toStdString(),new mapaRegistros(conpap,name.toStdString(),py)," ");
           ContenedorPlacas::instance().crearBoard(name.toStdString(),conpap,name.toStdString(),py," ");
           //*****************crear registros de las placas*****************************
           QRegularExpression re(R"(((?:0x)[0-9a-f]+|[0-9]+)?,\s*([0-9]+)?,\s*([a-z]+[0-9a-z_]*),\s*(On[a-z0-9_]+Access)?,\s*(On[a-z0-9_]+Read)?,\s*(On[_a-z0-9]+Write)?,\s*(On[_a-z0-9]+Exit)?)",QRegularExpression::MultilineOption|QRegularExpression:: CaseInsensitiveOption);
           QRegularExpressionMatchIterator i = re.globalMatch(openFile(k.absoluteFilePath()));
           while(i.hasNext()){
               QRegularExpressionMatch match = i.next();
               if(match.captured(1).isEmpty()){
                 ContenedorPlacas::instance().getRegister(name.toStdString())->crearRegName(match.captured(3).toStdString(),
                                                                         match.captured(4).toStdString(),
                                                                         match.captured(5).toStdString(),
                                                                         match.captured(6).toStdString(),
                                                                         match.captured(7).toStdString());
              }
              else{
                   ContenedorPlacas::instance().getRegister(name.toStdString())->crearRegAddress(match.captured(1).toInt(),
                                                                            match.captured(3).toStdString(),
                                                                            match.captured(2).toInt(),
                                                                            match.captured(4).toStdString(),
                                                                            match.captured(5).toStdString(),
                                                                            match.captured(6).toStdString(),
                                                                            match.captured(7).toStdString());
               }

           }
           //***********************start timer**********************************************
            if(tareas.count(name)==1){
                 ContenedorPlacas::instance().getRegister(name.toStdString())->startClock(tareas[name]);
           }
       }
   }
   //***********bus de comunicacion**********
   interfLow=new Interfazcfcc(py);
}
//*******metodos para crear proyecto, abrir proy, crear archivo, clonar archivo, borrar arch**//
QByteArray MainWindow::openFile(QString name)
{
    QByteArray fileRead;
    QFile file(name);
    if (!file.open(QIODevice::ReadOnly)){
          QMessageBox::information(this, tr("Unable to open file"),
          file.errorString());
    }
    fileRead=file.readAll();
    file.close();
    return fileRead;
}
//************************************************
void MainWindow::crearArchivo(QString Name)
{
    QFile file(Name);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, tr("Unable to open file"),
            file.errorString());
        return;
    }
    file.close();
}
//*****************************************
void MainWindow::abrirProyecto(QString path)
{
    dir = path;
    if(!dir.isEmpty()){
       model.setRootPath(dir);
       ui->treeView->setModel(&model);
       ui->treeView->setRootIndex(model.index(dir));
       for (int i=1;i<4;i++)
            ui->treeView->hideColumn(i);

       ui->treeView->header()->hide();
       ui->actionPlay->setEnabled(true);
       ui->actionBorrar->setEnabled(true);
       namePap="conexiones";
       pathPap=dir+"\\"+namePap+".csv";
       nameTareas="tareas";
       //pathTareas=dir+"\\"+nameTareas+".csv";
       pathTareas=dir+"\\"+nameTareas+".csv";
       Output::instance().createFileLog(dir);

   }
}
//abre o crea proyecto
void MainWindow::on_actionAbrir_Proyecto_triggered()
{
     QString path = QFileDialog::getExistingDirectory(this, tr("Proyecto"),
                                                    QDir::homePath(), QFileDialog::ShowDirsOnly);
     abrirProyecto(path);
}
//*************************************
//crea proyecto y/o archivo
void MainWindow::on_actionArchivo_nuevo_triggered()
{
    QString name;
    bool nameExist=false;
    if(dir.isEmpty())
        on_actionAbrir_Proyecto_triggered();
    bool ok;
    QString text = QInputDialog::getText(this, tr("Placa"),
                                           tr("Nombre:"), QLineEdit::Normal,"", &ok);

    //controla si el nombre de archivo ya existe
    QStringList filters;
    filters << "*.csv";
    QFileInfoList names = QDir(dir).entryInfoList(filters);
    for(const auto& k:names){
        name=k.baseName();
        if(name==text){
            nameExist=true;
            QMessageBox::information(this, tr("Aviso"),
                "Placa ya existe");
        }
    }
    if (ok && !text.isEmpty() && !nameExist){
       int indexx=ui->Placa->addTab(new PlacaWidget(this),text);
       ui->Placa->setCurrentIndex(indexx);
       QString Name;
       Name=dir+"\\"+text+".csv";
       crearArchivo(Name);
       Name=dir+"\\"+text+".py";
       crearArchivo(Name);
       //*********agrego texto al py*******************
       QString textoEditor;
       QsciScintilla *editor;
       editor=ui->Placa->widget(indexx)->findChild<QsciScintilla *>("editorTex");
       textoEditor="class varg:\n\tpack0=0\n\n";
       editor->append(textoEditor);
       textoEditor=QString("def capadecomunicacion(dato):\n\tlog('Hola mundo C, soy %1 en Python!')\n\treturn (varg.pack0,)\n\n").arg(text);
       editor->append(textoEditor);
       textoEditor="def tarea(contador, periodo):\n\tpass\n\n";
       editor->append(textoEditor);
       editor->SendScintilla(QsciScintilla::SCI_GOTOPOS, editor->length());
       //***********tabla de tarea***********
       QTableWidgetItem *item;
       int row;
       QTableWidget *tabla=dialogTest->findChild<QTableWidget*>("tableTarea");
       row=tabla->rowCount();
       tabla->insertRow(row);
       item = new QTableWidgetItem(text);
       tabla->setItem(row, 0, item);
       item = new QTableWidgetItem("0");
       tabla->setItem(row, 1, item);
       saveTareas();
       //**************************
       on_actionGuardar_triggered();
       //botones
       ui->actionClonar->setEnabled(true);
       ui->actionGuardar->setEnabled(true);
       ui->actionGuardar_todos->setEnabled(true);
    }
}
//****************************************
void MainWindow::on_actionClonar_triggered()
{
    if(!dir.isEmpty()){
        bool ok;
        QString text = QInputDialog::getText(this, tr("Placa"),
                                               tr("Nombre:"), QLineEdit::Normal,"", &ok);

        if (ok && !text.isEmpty()){
            int indexx=ui->Placa->addTab(new PlacaWidget(this),text);
            QString Name;
            Name=dir+"\\"+text+".csv";
            crearArchivo(Name);
            Name=dir+"\\"+text+".py";
            crearArchivo(Name);

            QTableWidget * tabla;
            tabla=ui->Placa->widget(ui->Placa->currentIndex())->findChild<QTableWidget *>("tabla");
            QsciScintilla *editor;
            editor=ui->Placa->widget(ui->Placa->currentIndex())->findChild<QsciScintilla *>("editorTex");
            ui->Placa->setCurrentIndex(indexx);
            QTableWidget * tablaClon;
            tablaClon=ui->Placa->widget(ui->Placa->currentIndex())->findChild<QTableWidget *>("tabla");
            QsciScintilla *editorClon;
            editorClon=ui->Placa->widget(ui->Placa->currentIndex())->findChild<QsciScintilla *>("editorTex");
            QTableWidgetItem *item;
            modif=false;
            for(int i=0;i<tabla->rowCount();i++){
                tablaClon->insertRow(i);
                for(int j=0;j<tabla->columnCount();j++){
                    text=tabla->item(i,j)->text();
                    item = new QTableWidgetItem(text.trimmed());
                    tablaClon->setItem(i, j, item);

                }
            }
            text=editor->text();
            editorClon->setText(text);
            modif=true;
            on_actionGuardar_triggered();
        }
    }
}
//**********************************************
void MainWindow::on_actionBorrar_triggered()
{
    QModelIndex index;
    QTableWidget *tabla;
    index=ui->treeView->currentIndex();
    QString ext= model.fileInfo(index).suffix();
    if(ext=="txt"){
        QFile file(model.fileInfo(index).absoluteFilePath());
        bool e=file.remove();
        if(!e)
            Output::instance().setWarning(file.errorString().toStdString());
    }
    else{
        QString name= model.fileInfo(index).baseName();
        if(name==namePap){
            QDir(dir).remove(name+".csv");
            tabla=DialogPap->findChild<QTableWidget *>("PaPTabla");
            int row=tabla->rowCount()-1;
            for(int i=row;i>=0;i--){
                tabla->removeRow(i);
            }
        }
        else if(name == nameTareas){
            QDir(dir).remove(name+".csv");
            tabla=dialogTest->findChild<QTableWidget *>("tableTarea");
            int row=tabla->rowCount()-1;
            for(int i=row;i>=0;i--){
                tabla->removeRow(i);
            }
        }
        else{
            QString namePy;
            QString nameCsv;
            namePy=name+".py";
            QDir(dir).remove(namePy);
            nameCsv=name+".csv";
            QDir(dir).remove(nameCsv);
            int count = ui->Placa->count();
            for(int i =0;i<count;i++){
                QString nametab = ui->Placa->tabText(i);
                if(name==nametab)
                    ui->Placa->removeTab(i);
            }
            tabla=dialogTest->findChild<QTableWidget *>("tableTarea");
            int row=tabla->rowCount()-1;
            for(int i=row;i>=0;i--){
                if(tabla->item(i,0)->text()==name)
                    tabla->removeRow(i);
            }

        }
    }
}
//**********************abre archivo desde el directorio*******************
void MainWindow::on_treeView_doubleClicked(const QModelIndex &index)
{
   QString item;
   int indexx;
   bool nameExist=false;
   QString namepy;
   QString namecsv;
   QString text;
   QString name=model.fileInfo(index).baseName();
   if(name==namePap){
       editorPap();
    }
   else if(name == nameTareas){
       editorTareas();
   }
   else{
       namecsv=dir+"\\"+name+".csv";
       namepy=dir+"\\"+name+".py";
       indexx=ui->Placa->count();
       for(int i=0;i<indexx;i++){
            item=ui->Placa->tabText(i);
            if(name==item){
                nameExist=true;
            }
       }
       if(!nameExist){
           modif=false;
           QRegularExpression re(R"(((?:0x)[0-9a-f]+|[0-9]+)?,\s*([0-9]+)?,\s*([a-z]+[0-9a-z_]*),\s*(On[a-z0-9_]+Access)?,\s*(On[a-z0-9_]+Read)?,\s*(On[a-z0-9_]+Write)?,\s*(On[a-z0-9_]+Exit)?)",QRegularExpression::MultilineOption|QRegularExpression:: CaseInsensitiveOption);
           QRegularExpressionMatchIterator i = re.globalMatch(openFile(namecsv));
           QTableWidget *tabla;
           QTableWidgetItem *item;
           int indexx=ui->Placa->addTab(new PlacaWidget(this),name);
           ui->Placa->setCurrentIndex(indexx);
           tabla=ui->Placa->widget(indexx)->findChild<QTableWidget *>("tabla");
           int row=0;
           while(i.hasNext()){
               QRegularExpressionMatch match = i.next();
               tabla->insertRow(row);
               for(int i=0;i<7;i++){
                   if(match.captured(i+1).isNull())
                       item = new QTableWidgetItem();
                    else{
                       text=match.captured(i+1);
                       item = new QTableWidgetItem(text.trimmed());
                   }
                    tabla->setItem(row, i, item);

                }
               row++;
           }
           QsciScintilla *editor;
           editor=ui->Placa->widget(ui->Placa->currentIndex())->findChild<QsciScintilla *>("editorTex");
           editor->setText(openFile(namepy));
           modif=true;
       }

       //botones
       ui->actionGuardar->setEnabled(true);
       ui->actionGuardar_todos->setEnabled(true);
       ui->actionClonar->setEnabled(true);
   }
}
//**********************control de archivos*******************************************
bool MainWindow::controlTabla()
{
    QRegularExpression re(R"(^((?:0x)?[0-9a-f]+,\s*[0-9]+,\s*[a-z]+[0-9a-z_]*,\s*|,\s*,\s*[a-z]+[0-9a-z_]*,\s*)(On[_a-z0-9_]+Access)?,\s*(On[_a-z0-9_]+Read)?,\s*(On[_a-z0-9_]+Write)?,\s*(On[_a-z0-9_]+Exit)?$)",QRegularExpression:: CaseInsensitiveOption);
    QRegularExpressionValidator v(re,0);
    int pos = 0;
    int state;
    QStringList filters;
    filters << "*.csv";
    QFileInfoList names = QDir(dir).entryInfoList(filters);
    for(const auto& k:names){
        QString fileRead;
        if(k.baseName()!=namePap && k.baseName()!=nameTareas){
            QFile file(k.absoluteFilePath());
            if (!file.open(QIODevice::ReadOnly)) {
                QMessageBox::information(this, tr("Unable to open file"),
                    file.errorString());
            }
            while(!file.atEnd()){
                fileRead=QString(file.readLine()).trimmed();
                state=v.validate(fileRead,pos);
                 if(state!=2){
                    QString log=QString("Error Sintactico en la tabla: %1  %2").arg(k.baseName()).arg(fileRead);
                    Output::instance().setError(log.toStdString());
                    return false;

                }
            }
            file.close();
        }
    }
    return true;
}
//*********************************************
bool MainWindow::controlPy()
{
    bool pyOk=true;
    QStringList filters;
    filters << "*.py";
    QFileInfoList names = QDir(dir).entryInfoList(filters);
    for(const auto& k:names){
        QString name=k.baseName();
        pyOk=py->crearModulo(name.toStdString(),QString (openFile(k.absoluteFilePath())).toStdString());
        if(!pyOk){
            Output::instance().setError("Error Sintactico en el script de Python: "+name.toStdString());
            pyOk=false;
            return pyOk;
        }
    }
    return pyOk;

}
//*********************************************************
bool MainWindow::controlTarea(){
    QRegularExpression re(R"(([a-z]+[0-9a-z_]+),\s*([0-9]+))",QRegularExpression::MultilineOption|QRegularExpression::CaseInsensitiveOption);
    QRegularExpressionMatchIterator i = re.globalMatch(openFile(pathTareas));
    QString placa;
    int tiempo;
    while(i.hasNext()){
        QRegularExpressionMatch match = i.next();
        placa=match.captured(1);
        tiempo=match.captured(2).toInt();
        //si no existe la placa
        if(tareas.count(placa)!=1){
            //si el tiempo es distinto a cero
            if(tiempo !=0)
              tareas[placa]=tiempo;
        }
        else{
            Output::instance().setError("placa repetida: "+placa.toStdString());
            return false;
        }

    }
    return true;

}
//********************************************************
bool MainWindow::controlPap()
{
    list<string>conex_reg;
    list<string>conex_bit;
    QRegularExpression re(R"(([a-z]+[0-9a-z_]*),\s*((?:0x)?[0-9a-f]+|[a-z]+[0-9a-z_]*),\s*([0-9]+),\s*([([a-z]+[0-9a-z_]*),\s*((?:0x)?[0-9a-f]+|[a-z]+[0-9a-z_]*),\s*([0-9]+))",QRegularExpression:: CaseInsensitiveOption);
    QRegularExpressionValidator v(re,0);
    int pos = 0;
    int state;

    QString fileRead;
    QFile file(pathPap);
    if (!file.open(QIODevice::ReadOnly)){
        QMessageBox::information(this, tr("Unable to open file"),
            file.errorString());
    }
    while(!file.atEnd()){
        fileRead=QString(file.readLine().trimmed());
        state=v.validate(fileRead,pos);
        if(state==2)
            conex_bit.push_back(fileRead.toStdString());
        else{
            re.setPattern(R"(([a-z]+[0-9a-z_]+),\s*((?:0x)?[0-9a-f]+|[a-z]+[0-9a-zA-Z_]*),\s*,\s*([([a-z]+[0-9a-z_]+),\s*((?:0x)?[0-9a-f]+|[a-z]+[0-9a-zA-Z_]*),)");
            v.setRegularExpression(re);
            state=v.validate(fileRead,pos);
            if(state==2)
                conex_reg.push_back(fileRead.toStdString());
            else{
                Output::instance().setError("Error Sintaxtico en conexiones entre placas: "+fileRead.toStdString());
                return false;
            }
        }
    }
    file.close();
    conpap= new ConexionesPaP(conex_reg,conex_bit);
    return true;
}
//*******+metodos referidos a guardar*****************//
void MainWindow::guardarTabla(QString name, int index)
{
    QTableWidget *tabla;
    QTableWidgetItem *item;
    QString text;
    QFile file(name);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, tr("Unable to open file"),
            file.errorString());
        return;
    }
     QTextStream ts( &file );
     QStringList strList;
     tabla=ui->Placa->widget(index)->findChild<QTableWidget *>("tabla");
     for(int i=0;i<tabla->rowCount();i++){
         strList.clear();
         for(int j=0;j<tabla->columnCount();j++){
             item=tabla->item(i,j);
             if(item){
                 text=item->text();
                 strList<<text;
             }
         }
         ts << strList.join(", ") + "\n";
     }
     file.close();
}
//**************************************************
void MainWindow::guardarPython(QString name,int index)
{
    QFile file(name);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, tr("Unable to open file"),
            file.errorString());
        return;
    }
     QTextStream ts( &file );
     QsciScintilla *editor;
     editor=ui->Placa->widget(index)->findChild<QsciScintilla *>("editorTex");

     ts<<editor->text();
     file.close();
}
//************************************************
void MainWindow::guardar(int i)
{
    QString name;
    QString namecsv;
    QString namepy;
    name=ui->Placa->tabText(i);
    namecsv=dir+"\\"+name +".csv";
    guardarTabla(namecsv,i);
    namepy=dir+"\\"+name +".py";
    guardarPython(namepy,i);
}
//************************************************
int MainWindow::controlAsterisco(QString name)
{
    QRegularExpression re(R"(([a-z]+[0-9a-zA-Z_]*\*))");
    QRegularExpressionValidator v(re, 0);
    int pos=0;
    int state;
    state=v.validate(name, pos);
    return state;
}
//************************************************
//si el tab tiene asterisco se lo quita y habilita guardar si no verifica si hubo perdida de registros
bool MainWindow::controlGuardar(QString name, int index)
{
    QRegularExpression re;
    bool save=false;
    int state=controlAsterisco(name);
    if(state==2){
        re.setPattern(R"(([a-z]+[0-9a-zA-Z_]*))");
        name=re.match(name).captured(1);
        ui->Placa->setTabText(index,name);
        save=true;
    }
    return save;
}
//*****************************************************
void MainWindow::on_actionGuardar_triggered()
{
    int index=ui->Placa->currentIndex();
    QString name= ui->Placa->tabText(index);
    if(index>=0){
        if(controlGuardar(name,index)){
               guardar(index);
        }
    }
}
//************************************************
void MainWindow::on_actionGuardar_todos_triggered()
{
    QString name;
    int indexx=ui->Placa->count();
    for(int i=0;i<indexx;i++){
         name=ui->Placa->tabText(i);
         if(controlGuardar(name,i)){
            guardar(i);
        }
    }
}
//********************************
//guarda conex pap
void MainWindow::savepap()
{
    QString text;
    QTableWidget *tabla;
    QTableWidgetItem *item;
    tabla=DialogPap->findChild<QTableWidget*>("PaPTabla");
    QFile file(pathPap);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, tr("Unable to open file"),
            file.errorString());
        return;
    }
    QTextStream ts( &file );
    QStringList strList;
    for(int i=0;i<tabla->rowCount();i++){
        strList.clear();
        for(int j=0;j<tabla->columnCount();j++){
            item=tabla->item(i,j);
            if(item){
                text=item->text();
                strList<<text;
            }
        }
        ts << strList.join(", ") + "\n";
    }
    file.close();
}
//***************************************************
void MainWindow::saveTareas()
{
    //qDebug()<<"entro";
    QString text;
    QTableWidget *tabla;
    QTableWidgetItem *item;
    tabla=dialogTest->findChild<QTableWidget*>("tableTarea");
    QFile file(pathTareas);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, tr("Unable to open file"),
            file.errorString());
        return;
    }
    QTextStream ts( &file );
    QStringList strList;
    for(int i=0;i<tabla->rowCount();i++){
        strList.clear();
        for(int j=0;j<tabla->columnCount();j++){
            item=tabla->item(i,j);
            if(item){
                text=item->text();
                strList<<text;
            }
        }
        ts << strList.join(", ") + "\n";
    }
    file.close();
}
//************************************************
void MainWindow::limpiarEditorLog()
{
    ui->editorlog->clear();
}
//******************************************************
void MainWindow::disableIconData(){
    ui->tranfDatos->setStyleSheet("background-image: url(:/imagenes/link-inactive.svg)");
}
//*****************************************************
void MainWindow::EditorLog(QString text,QString color )
{

    ui->tabsalida->setCurrentWidget(ui->tab_log);
    ui->tranfDatos->setStyleSheet("background-image: url(:/imagenes/link-active.svg)");
    QTextCursor c = ui->editorlog->textCursor();
    c.movePosition(QTextCursor::End);
    c.insertHtml(QString("<span style=\"color: %2\">%1</span><br>")
                         .arg(text)
                         .arg(color));

    if(c.position()>10000){
       ui->editorlog->clear();
    }
    ui->editorlog->setTextCursor(c);

   if(flagConr){
       conr->publicar(text);
   }
   QTimer::singleShot(2000, this, SLOT(disableIconData()));
}
//*****************************************************
void MainWindow::EditorComp(QString text,QString color)
{
    ui->tabsalida->setCurrentWidget(ui->tab_salidacomp);
    QTextCursor c = ui->editorsc->textCursor();
    c.movePosition(QTextCursor::End);
    c.insertHtml(QString("<span style=\"color: %2\">%1</span><br>")
                         .arg(text)
                         .arg(color));
    ui->editorsc->setTextCursor(c);
    if(flagConr){
        conr->publicar(text);
    }
}
//****************************************************
void MainWindow::KillLog(QString text, QString color)
{
    ui->tabsalida->setCurrentWidget(ui->tab_log);
    QTextCursor c = ui->editorlog->textCursor();
    c.movePosition(QTextCursor::End);
    c.insertHtml(QString("<span style=\"color: %2\">%1</span><br>")
                         .arg(text)
                         .arg(color));
    ui->editorlog->setTextCursor(c);
    if(flagConr){
        conr->publicar(text);
    }
    on_actionStop_triggered();
}
//**********************************************
void MainWindow::resizeEvent(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    int index=ui->Placa->count();
    for(int i=0;i<index;i++){
       QsciScintilla *editor;
       editor=ui->Placa->widget(i)->findChild<QsciScintilla *>("editorTex");
       int ancho=editor->width();
       CornerWidget *c =(CornerWidget*)ui->Placa->cornerWidget(Qt::TopRightCorner);
       QLineEdit * lineEdit=c->findChild<QLineEdit *>("lineEdit");
       lineEdit->setMinimumWidth(ancho-153);
    }
}
//*************metodos para cerrar los tab****************
bool MainWindow::mjsSave()
{
    bool var=true;
    QMessageBox msgBox;
    msgBox.setText("The document has been modified.");
    msgBox.setInformativeText("Do you want to save your changes?");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();
    switch (ret) {
      case QMessageBox::Save:
          var=true;
          break;
      case QMessageBox::Discard:
          var=false;
          break;
      case QMessageBox::Cancel:
          var=false;
          break;
      default:
          var=false;
          break;
    }
    return var;
}
//**********************************************
void MainWindow::eliminartodo()
{
    int count = ui->Placa->count()-1;
    for(int i=count; i>=0 ;i--){
        on_Placa_tabCloseRequested(i);
    }
}
//**********************************************
void MainWindow::on_Placa_tabCloseRequested(int index)
{
    QString name= ui->Placa->tabText(index);
    if(controlGuardar(name,index)){
       if(mjsSave())
           guardar(index);
     }
    ui->Placa->removeTab(index);
}
//*****************evento de consola*****************************
void MainWindow::on_consola_returnPressed()
{
    QString comando=ui->consola->text();
    if(!comando.isEmpty())
        InterfazCom::instance().ejecutaComando(comando.toStdString());
    ui->consola->clear();
}
//abrir pap
void MainWindow::abrirPap(){
    QString text;
    QTableWidget* tabla;
    QTableWidgetItem *item;
    tabla=DialogPap->findChild<QTableWidget*>("PaPTabla");
    //elimino tabla por si hay algo y abro de cero la primera vez

    int row=tabla->rowCount()-1;
    for(int i=row;i>=0;i--){
        tabla->removeRow(i);
    }

    QRegularExpression re(R"(([a-z]+[0-9a-z_]+),\s*((?:0x)?[0-9a-f]+|[a-z]+[0-9a-z_]*),\s*([0-9]*),\s*([([a-z]+[0-9a-z_]+),\s*((?:0x)?[0-9a-f]+|[a-z]+[0-9a-z_]*),\s*([0-9]*))",QRegularExpression::MultilineOption|QRegularExpression::CaseInsensitiveOption);
    QRegularExpressionMatchIterator i = re.globalMatch(openFile(pathPap));
    row=0;
    while(i.hasNext()){
        QRegularExpressionMatch match = i.next();
        tabla->insertRow(row);
        for(int i=0;i<6;i++){
            if(match.captured(i+1).isNull())
                item = new QTableWidgetItem();
             else{
                text=match.captured(i+1);
                item = new QTableWidgetItem(text.trimmed());
            }
             tabla->setItem(row, i, item);
         }
        row++;
    }
}
//****************************************************
void MainWindow::abrirTareas()
{
    QString text;
    QTableWidget* tabla;
    QTableWidgetItem *item;
    tabla=dialogTest->findChild<QTableWidget*>("tableTarea");
    //elimino tabla por si hay algo y abro de cero la primera vez
    int row=tabla->rowCount()-1;
    for(int i=row;i>=0;i--){
        tabla->removeRow(i);
    }
    QRegularExpression re(R"(([a-z]+[0-9a-z_]+),\s*([0-9]+))",QRegularExpression::MultilineOption|QRegularExpression::CaseInsensitiveOption);
    QRegularExpressionMatchIterator i = re.globalMatch(openFile(pathTareas));
    row=0;
    while(i.hasNext()){
        QRegularExpressionMatch match = i.next();
        tabla->insertRow(row);
        for(int i=0;i<2;i++){
            if(match.captured(i+1).isNull())
                item = new QTableWidgetItem();
             else{
                text=match.captured(i+1);
                item = new QTableWidgetItem(text.trimmed());
            }
             tabla->setItem(row, i, item);
         }
        row++;
    }
}
//*************evento de mostrar el pupup de las conex************
void MainWindow::editorPap()
{
    if(dir.isNull()){
        on_actionAbrir_Proyecto_triggered();
    }
    //existe un proyecto, existe las conexiones pero no se abrieron nunca
    if(!dir.isNull()&&QFileInfo(QDir(dir).absoluteFilePath(pathPap)).exists()&&papOpen){
        abrirPap();
        papOpen=false;
    }
    DialogPap->exec();
}
//*****************************************************
void MainWindow::editorTareas()
{
    if(dir.isNull()){
        on_actionAbrir_Proyecto_triggered();
    }
    //existe un proyecto, existe las conexiones pero no se abrieron nunca
    if(!dir.isNull()&&QFileInfo(QDir(dir).absoluteFilePath(pathTareas)).exists()&&tareaOpen){
        abrirTareas();
        tareaOpen=false;
    }
    dialogTest->exec();
}
//**************************EPP*******************
void MainWindow::puertoEpp()
{
    QString portLPT="0x378"; //hacer global
    ui->lptPortEdit->setText(portLPT);
    QString pathexe=QCoreApplication::applicationDirPath();
    QString pathfile=QString(R"(%1\Config\inpout32.ini)").arg(pathexe); //variable global
    QFile file(pathfile);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(this, tr("Unable to open file"),
            file.errorString());
        return;
    }
    QByteArray fileRead=file.readAll();
    file.close();
    QRegularExpression re(R"(mode = 0\r\nurl = tcp:\/\/([0-9]+.[0-9]+.[0-9]+.[0-9]+:[0-9]+))",QRegularExpression::MultilineOption);
    QRegularExpressionMatchIterator i = re.globalMatch(fileRead);
    if(i.hasNext()){
        QRegularExpressionMatch match = i.next();
        ui->ip_port_tcpEdit->setText(match.captured(1));
    }
}
//********************************************
void MainWindow::on_lptPortEdit_returnPressed()
{
    QString portLPT=ui->lptPortEdit->text(); //variable global
}
//**************************************************************
void MainWindow::on_ip_port_tcpEdit_returnPressed()
{
    QString pathexe=QCoreApplication::applicationDirPath();
    QString pathfile=QString("%1\\Config\\inpout32.ini").arg(pathexe); //variable global
    QFile file(pathfile);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, tr("Unable to open file"),
            file.errorString());
        return;
    }
    QString tcp=ui->ip_port_tcpEdit->text();
    QRegularExpression re(R"(([0-9]+.[0-9]+.[0-9]+.[0-9]+:[0-9]+))");
    QRegularExpressionValidator v(re,0);
    int pos = 0;
    int state;
    state=v.validate(tcp,pos);
    if(state==2){
        //QString tcp_=QString("mode = 0%2url = tcp://%1").arg(tcp).arg("\r\n");
        //QTextStream ts( &file );
        //qDebug()<<tcp_;
        //ts<<tcp_;
        ;
    }
    else
        Output::instance().setError("ip y/o Puerto incorrecto");
}
//**************control remoto******************
void MainWindow::conectarConsola(bool check)
{
    if(check){
        conr->conectar(ip);
        flagConr=true;
        controlConec();
    }
    else{
        conr->desconectar();
        flagConr=false;
    }
}
//***********************************************++
void MainWindow::confIp()
{
    dialogIp->exec();
}
//****************************************************
void MainWindow::saveip()
{
    QLineEdit* editor;
    editor=dialogIp->findChild<QLineEdit*>("lineEdit");
    ip=editor->text();
}
//********************************************
void MainWindow::controlConec()
{
    if(!conr->isConnected()&&flagConr){
        conr->conectar(ip);
        QTimer::singleShot(2000, this, SLOT(controlConec()));
    }
}





