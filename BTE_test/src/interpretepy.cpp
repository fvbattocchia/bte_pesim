#include "interpretepy.h"
#define _hypot hypot
#include <Python.h>
#include "ContenedorPlacas.h"
#include "mapaRegistros.h"
#include "Output.h"
#include <QDebug>
#include <QString>
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
InterpretePy::InterpretePy()
{
    // initialize Python
    Py_InitializeEx(0) ;

}
static void check_py_error(PyObject* pyObj, string error)
{
    if(PyErr_Occurred() != NULL) {
        PyObject *pyExcType;
        PyObject *pyExcValue;
        PyObject *pyExcTraceback;
        PyErr_Fetch(&pyExcType, &pyExcValue, &pyExcTraceback);
        PyErr_NormalizeException(&pyExcType, &pyExcValue, &pyExcTraceback);

        PyObject* str_exc_type = PyObject_Repr(pyExcType);
        PyObject* pyStr = PyUnicode_AsEncodedString(str_exc_type, "utf-8", "Error ~");
        const char *strExcType =  PyBytes_AS_STRING(pyStr);

        PyObject* str_exc_value = PyObject_Repr(pyExcValue);
        PyObject* pyExcValueStr = PyUnicode_AsEncodedString(str_exc_value, "utf-8", "Error ~");
        const char *strExcValue =  PyBytes_AS_STRING(pyExcValueStr);

        // When using PyErr_Restore() there is no need to use Py_XDECREF for these 3 pointers
        //PyErr_Restore(pyExcType, pyExcValue, pyExcTraceback);


        Py_XDECREF(pyExcType);
        Py_XDECREF(pyExcValue);
        Py_XDECREF(pyExcTraceback);

        Py_XDECREF(str_exc_type);
        Py_XDECREF(pyStr);

        Py_XDECREF(str_exc_value);
        Py_XDECREF(pyExcValueStr);
        throw string(strExcType)+","+ string(strExcValue);
        }
        else{
            if(pyObj==NULL){
                throw error;
            }
        }
}
//extension de python
//las funciones de c llamas desde python
/* Return the number of arguments of the application command line */
static PyObject*
emb_write(PyObject *self, PyObject *args)
{
    bool check= false;
    unsigned long long address;
    char *name;
    unsigned long long dato;
    PyObject *namePyboard=PyObject_GetAttrString(self,"placa");
    const char* nameCboard= PyUnicode_AsUTF8(namePyboard);
    string nameb=nameCboard;

    if(PyArg_ParseTuple(args, "KK",&address,&dato)){
        ContenedorPlacas::instance().getRegister(nameb)->write(address,dato,true);
    }
    else if(PyArg_ParseTuple(args, "sK",&name,&dato)){
        PyErr_Clear();
        ContenedorPlacas::instance().getRegister(nameb)->write(name,dato,true);
    }
    else
        check=true;

    Py_DecRef(namePyboard);
    if(check){
        Output::instance().setLogError(QString("Fallo al escribir en la placa: %1 converion de datos invalida").arg(nameb.c_str()).toStdString());
        return(NULL);
    }
    else
        return PyLong_FromLong(0);

}
/* Return the number of arguments of the application command line */
static PyObject*
emb_read(PyObject *self, PyObject *args)
{
    bool check= false;
    unsigned long long address;
    char *name;
    uint64_t dato;
    PyObject *namePyboard=PyObject_GetAttrString(self,"placa");
    const char* nameCboard= PyUnicode_AsUTF8(namePyboard);
    string nameb=nameCboard;

    if(PyArg_ParseTuple(args, "K",&address)){
        dato=ContenedorPlacas::instance().getRegister(nameb)->read(address,true);
    }
    else if(PyArg_ParseTuple(args, "s",&name)){
        PyErr_Clear();
        dato=ContenedorPlacas::instance().getRegister(nameb)->read(name,true);
    }
    else
        check=true;

    Py_DecRef(namePyboard);
    if(check){
         Output::instance().setLogError(QString("Fallo al leer la placa: %1 converion de datos invalida").arg(nameb.c_str()).toStdString());
        return(NULL);
    }
    else
        return PyLong_FromLong(dato);
}
//************************escribe y lee en otra placa***************************************
/* Return the number of arguments of the application command line */
static PyObject*
emb_write_board(PyObject *self, PyObject *args)
{
    bool check= false;
    unsigned long long address;
    char *name;
    char *namebo;
    unsigned long long dato;
    PyObject *namePyboard=PyObject_GetAttrString(self,"placa");
    const char* nameCboard= PyUnicode_AsUTF8(namePyboard);
    string nameb=nameCboard;

    if(PyArg_ParseTuple(args, "sKK",&namebo,&address,&dato)){
        ContenedorPlacas::instance().getRegister(namebo)->write(address,dato,true);
    }
    else if(PyArg_ParseTuple(args, "ssK",&namebo,&name,&dato)){
        PyErr_Clear();
        ContenedorPlacas::instance().getRegister(namebo)->write(name,dato,true);
    }
    else
        check=true;

    Py_DecRef(namePyboard);
    if(check){
        Output::instance().setLogError(QString("Fallo al escribir en la placa: %1 converion de datos invalida").arg(nameb.c_str()).toStdString());
        return(NULL);
    }
    else
        return PyLong_FromLong(0);

}
/* Return the number of arguments of the application command line */
static PyObject*
emb_read_board(PyObject *self, PyObject *args)
{
    bool check= false;
    unsigned long long address;
    char *name;
    char *namebo;
    uint64_t dato;
    PyObject *namePyboard=PyObject_GetAttrString(self,"placa");
    const char* nameCboard= PyUnicode_AsUTF8(namePyboard);
    string nameb=nameCboard;

    if(PyArg_ParseTuple(args, "sK",&namebo,&address)){
        dato=ContenedorPlacas::instance().getRegister(namebo)->read(address,true);
    }
    else if(PyArg_ParseTuple(args, "ss",&namebo,&name)){
        PyErr_Clear();
        dato=ContenedorPlacas::instance().getRegister(namebo)->read(name,true);
    }
    else
        check=true;

    Py_DecRef(namePyboard);
    if(check){
         Output::instance().setLogError(QString("Fallo al leer la placa: %1 converion de datos invalida").arg(nameb.c_str()).toStdString());
        return(NULL);
    }
    else
        return PyLong_FromLong(dato);
}
//***********************************************************
/* Return the number of arguments of the application command line */
static PyObject*
emb_log(PyObject *self, PyObject *args)
{
    PyObject *namePyboard=PyObject_GetAttrString(self,"placa");
    const char* nameCboard= PyUnicode_AsUTF8(namePyboard);
    string nameb=nameCboard;
    char *buf;
    if(!PyArg_ParseTuple(args, "s",&buf)){
        Output::instance().setLogError(QString("Fallo en log, placa: %1 conversion de datos invalida").arg(nameb.c_str()).toStdString());
        return NULL;
    }

    string log=buf;
    log="LOG: "+nameb+": "+log;
    Output::instance().setLog(log);
    return PyLong_FromLong(0);
}
//tabla de funciones del modulo que relacional las de python y c
static PyMethodDef EmbMethods[] = {
    {"write", emb_write, METH_VARARGS,"llama a escribir registros"},
    {"read", emb_read, METH_VARARGS,"llama a leer registros"},
    {"write_board", emb_write_board, METH_VARARGS,"llama a escribir registros de otra placa"},
    {"read_board", emb_read_board, METH_VARARGS,"llama a leer registros de otra placa"},
    {"log", emb_log, METH_VARARGS,"loggea"},
    {NULL, NULL, 0, NULL}
};
//embeber python en c
bool InterpretePy::crearModulo(string placa, string script)
{
    try{
       // compile our function
        PyObject* pCompiledFn = Py_CompileString( script.c_str() , "" , Py_file_input ) ;
        check_py_error(pCompiledFn,QString("Fallo al compilar Python: %1").arg(placa.c_str()).toStdString());
       // create a module
        PyObject* pModule = PyImport_ExecCodeModule( placa.c_str() , pCompiledFn ) ;
       check_py_error(pModule, QString("Fallo al crear el modulo: %1").arg(placa.c_str()).toStdString());
        modulos[placa]=pModule;
        cout<<placa<<","<<modulos[placa]<<endl;
        PyModule_AddStringConstant(pModule,"placa",placa.c_str());
        //le agrego al modulo la tabla de funciones
        PyModule_AddFunctions(pModule,EmbMethods);
       // Py_DecRef(pModule);
        Py_DecRef(pCompiledFn);
        return true;
   }
    catch(string e){
        Output::instance().setError(e);
        return false;
    }
}

uint64_t InterpretePy::write(string placa, string funcion, uint64_t dato, bool var, string name)
{
    try{
        // locate the "add" function (it's an attribute of the module)
        PyObject* pAddFn = PyObject_GetAttrString(modulos[placa] ,funcion.c_str()) ;
        check_py_error(pAddFn,QString("Puntero pAddFn null: %2, %1").arg(funcion.c_str()).arg(placa.c_str()).toStdString());

        // create a new tuple with 1 elements
        PyObject* pPosArgs = PyTuple_New( 1 ) ;

        // convert the first command-line argument to an int, then put it into the tuple
        PyObject* pVal1 = PyLong_FromLong(dato);
        check_py_error(pVal1,QString("Puntero pVal1 null: %2, %1").arg(funcion.c_str()).arg(placa.c_str()).toStdString());
        PyTuple_SetItem( pPosArgs , 0 ,  pVal1 ) ; // nb: tuple position 0
        // create a new dictionary
        PyObject* pKywdArgs = PyDict_New() ;
        // call our function

        PyObject* pResult = PyObject_Call( pAddFn , pPosArgs , pKywdArgs ) ;
        check_py_error(pResult,QString("Puntero pResult null: %2, %1").arg(funcion.c_str()).arg(placa.c_str()).toStdString());

        Py_DecRef( pResult ) ;
        Py_DecRef( pKywdArgs );
        //No need to decref pVal1 and pVal2 - the tuple has ownership of them.
        Py_DecRef( pPosArgs ) ;
        Py_DecRef( pAddFn ) ;
        if(var){
            QString log=QString("ESCRITURA: %1 %2%4 %3")
                    .arg(placa.c_str())
                    .arg(name.c_str(),-24,' ')
                    .arg(to_string(dato).c_str())
                    .arg("|DATO:",-9,' ');
            Output::instance().setLog(log.toStdString());
        }
    }
    catch(string e){
        Output::instance().setLogError(e);
    }

    return 0;
}
uint64_t InterpretePy::write(string placa, string funcion,uint64_t address, uint64_t dato,bool var,string name)
{

    try{
        // locate the "add" function (it's an attribute of the module)
        PyObject* pAddFn = PyObject_GetAttrString(modulos[placa] ,funcion.c_str()) ;
        check_py_error(pAddFn,QString("Puntero pAddFn null: %2, %1").arg(funcion.c_str()).arg(placa.c_str()).toStdString());

        // create a new tuple with 1 elements
        PyObject* pPosArgs = PyTuple_New( 2 ) ;

        // convert the first command-line argument to an int, then put it into the tuple
        PyObject* pVal1 = PyLong_FromLong(address);
        check_py_error(pVal1,QString("Puntero pVal1 null: %2, %1").arg(funcion.c_str()).arg(placa.c_str()).toStdString());
        PyTuple_SetItem( pPosArgs , 0 ,  pVal1 ) ; // nb: tuple position 0
        // convert the second command-line argument to an int, then put it into the tuple
        PyObject* pVal2 = PyLong_FromLong(dato);
        check_py_error(pVal2,QString("Puntero pVal2 null: %2, %1").arg(funcion.c_str()).arg(placa.c_str()).toStdString());
        PyTuple_SetItem( pPosArgs , 1 ,  pVal2 ) ; // nb: tuple position 0
        // create a new dictionary
        PyObject* pKywdArgs = PyDict_New() ;

        // call our function
        PyObject* pResult = PyObject_Call( pAddFn , pPosArgs , pKywdArgs ) ;
        check_py_error(pResult,QString("Puntero pResult null: %2, %1").arg(funcion.c_str()).arg(placa.c_str()).toStdString());

        Py_DecRef( pResult ) ;
        Py_DecRef( pKywdArgs ) ;
        //No need to decref pVal1 and pVal2 - the tuple has ownership of them.
        Py_DecRef( pPosArgs ) ;
        Py_DecRef( pAddFn ) ;
        if(var){
            QString log=QString("ESCRITURA: %1 %2%3 %4 %5 %6")
                    .arg(placa.c_str())
                    .arg(name.c_str(),-10,' ')
                    .arg("DIRECCION")
                    .arg(to_string(address).c_str())
                    .arg("|DATO:",-9,' ')
                    .arg(to_string(dato).c_str());



            Output::instance().setLog(log.toStdString());
        }
    }
    catch(string e){
        Output::instance().setLogError(e);
    }


    return 0;
}
uint64_t InterpretePy::read(string placa,string funcion,string name,bool var)
{
   try{
        PyObject* pAddFn = PyObject_GetAttrString(modulos[placa] ,funcion.c_str()) ;
        check_py_error(pAddFn,QString("Puntero pAddFn null: %2, %1").arg(funcion.c_str()).arg(placa.c_str()).toStdString());

        // create a new tuple with 0 elements
        PyObject* pPosArgs = PyTuple_New( 0 );

        // create a new dictionary
        PyObject* pKywdArgs = PyDict_New();

        // call our function
        PyObject* pResult = PyObject_Call( pAddFn , pPosArgs , pKywdArgs ) ;
        check_py_error(pResult,QString("Puntero pResult null: %2, %1").arg(funcion.c_str()).arg(placa.c_str()).toStdString());

        // convert the result to a uint
        uint64_t pResultRepr = PyLong_AsLongLong( pResult ) ;


        Py_DecRef( pResult ) ;
        Py_DecRef( pKywdArgs ) ;
        //No need to decref pVal1 and pVal2 - the tuple has ownership of them.
        Py_DecRef( pPosArgs ) ;
        Py_DecRef( pAddFn ) ;
        if(var){
            QString log=QString("LECTURA:  %1, %2%4 %3")
                    .arg(placa.c_str())
                    .arg(name.c_str(),-23,' ')
                    .arg(to_string(pResultRepr).c_str())
                    .arg("|RETORNO:");
            Output::instance().setLog(log.toStdString());
        }
        return pResultRepr;
    }
    catch(string e){
        Output::instance().setLogError(e);
        return 0;
    }
}
uint64_t InterpretePy::read(string placa, string funcion, uint64_t address, string name,bool var)
{
    try{
        PyObject* pAddFn = PyObject_GetAttrString(modulos[placa] ,funcion.c_str()) ;
        check_py_error(pAddFn,QString("Puntero pAddFn null: %2, %1").arg(funcion.c_str()).arg(placa.c_str()).toStdString());

        // create a new tuple with 1 elements
        PyObject* pPosArgs = PyTuple_New( 1 ) ;
        // convert the first command-line argument to an int, then put it into the tuple
        PyObject* pVal1 = PyLong_FromLong(address);
        check_py_error(pVal1,QString("Puntero pVal1 null: %2, %1").arg(funcion.c_str()).arg(placa.c_str()).toStdString());
        PyTuple_SetItem( pPosArgs , 0 ,  pVal1 ) ; // nb: tuple position 0
        // create a new dictionary
        PyObject* pKywdArgs = PyDict_New();
         // call our function
        PyObject* pResult = PyObject_Call( pAddFn , pPosArgs , pKywdArgs ) ;
        check_py_error(pResult,QString("Puntero pResult null: %2, %1").arg(funcion.c_str()).arg(placa.c_str()).toStdString());

        // convert the result to a uint
        uint64_t pResultRepr = PyLong_AsLongLong( pResult ) ;

        Py_DecRef( pResult ) ;
        Py_DecRef( pKywdArgs ) ;
        //No need to decref pVal1 and pVal2 - the tuple has ownership of them.
        Py_DecRef( pPosArgs ) ;
        Py_DecRef( pAddFn ) ;
        if(var){
            QString log=QString("LECTURA:   %1 %2%3 %4 %5 %6")
                    .arg(placa.c_str())
                    .arg(name.c_str(),-10,' ')
                    .arg( "DIRECCION:")
                    .arg(to_string(address).c_str())
                    .arg("|RETORNO:")
                    .arg(to_string(pResultRepr).c_str());
              Output::instance().setLog(log.toStdString());
        }
        return pResultRepr;
    }
    catch(string e){
        Output::instance().setLogError(e);
        return 0;
    }
}


list<uint64_t> InterpretePy::capaDeCom(string placa,list<uint64_t>datos)
{
    list<uint64_t> retorno;

    try{
         //locate the "add" function (it's an attribute of the module), modulo y nombre de fn
        PyObject* pAddFn = PyObject_GetAttrString(modulos[placa] ,"capadecomunicacion") ;
         check_py_error(pAddFn,QString("Puntero pAddFn null: %2, %1").arg("capadecomunicacion").arg(placa.c_str()).toStdString());
        // create a new tuple with 1 elements
        PyObject* pPosArgs = PyTuple_New(datos.size()) ;
        // convert the first command-line argument to an int, then put it into the tuple
        int j=0;
        for(const auto& i:datos){
            PyObject* pVal1 = PyLong_FromLong(i);
            check_py_error(pVal1,QString("Puntero pVal1 null: %2, %1").arg("capadecomunicacion").arg(placa.c_str()).toStdString());
            PyTuple_SetItem( pPosArgs , j ,  pVal1 ); // nb: tuple position 0
            j++;
        }
        PyObject* pPosArgst = PyTuple_Pack(1,pPosArgs);

        // create a new dictionary
        PyObject* pKywdArgs = PyDict_New();

        // call our function
        PyObject* pResult = PyObject_Call( pAddFn , pPosArgst , pKywdArgs );
        check_py_error(pResult,QString("Puntero pResult null: %2, %1").arg("capadecomunicacion").arg(placa.c_str()).toStdString());
        // convert the result to a list
        int sizeTuple = PyTuple_GET_SIZE(pResult);
        for(int i=0;i<sizeTuple;i++){
            retorno.push_back(PyLong_AsLong(PyTuple_GetItem(pResult,i)));
        }
        // clean up
        Py_DecRef( pResult ) ;
        Py_DecRef( pKywdArgs ) ;
        //No need to decref pVal1 and pVal2 - the tuple has ownership of them.
        Py_DecRef( pPosArgs ) ;
        Py_DecRef( pAddFn ) ;
        return retorno;
    }
    catch(string e){
        Output::instance().setLogError(e);
        return retorno;
    }
}

void InterpretePy::ejecutarTarea(string placa,uint64_t contador,int periodo)
{
    try{
         //locate the "add" function (it's an attribute of the module), modulo y nombre de fn
        PyObject* pAddFn = PyObject_GetAttrString(modulos[placa] ,"tarea") ;
        check_py_error(pAddFn,QString("Puntero pAddFn null: %2, %1").arg("tarea").arg(placa.c_str()).toStdString());

        // create a new tuple with 2 elements
        PyObject* pPosArgs = PyTuple_New( 2 ) ;

        // convert the first command-line argument to an int, then put it into the tuple
        PyObject* pVal1 = PyLong_FromLong(contador);
        check_py_error(pVal1,QString("Puntero pVal1 null: %2, %1").arg("tarea").arg(placa.c_str()).toStdString());
        PyTuple_SetItem( pPosArgs , 0 ,  pVal1 ) ; // nb: tuple position 0
        // convert the second command-line argument to an int, then put it into the tuple
        PyObject* pVal2 = PyLong_FromLong(periodo);
        check_py_error(pVal2,QString("Puntero pVal2 null: %2, %1").arg("tarea").arg(placa.c_str()).toStdString());
        PyTuple_SetItem( pPosArgs , 1 ,  pVal2 ) ; // nb: tuple position 0
        // create a new dictionary
        // create a new dictionary
        PyObject* pKywdArgs = PyDict_New();
        // call our function
        PyObject* pResult = PyObject_Call( pAddFn , pPosArgs , pKywdArgs );
        check_py_error(pResult,QString("Puntero pResult null: %2, %1").arg("tarea").arg(placa.c_str()).toStdString());
        // clean up
        Py_DecRef( pResult ) ;
        Py_DecRef( pKywdArgs ) ;
        //No need to decref pVal1 and pVal2 - the tuple has ownership of them.
        Py_DecRef( pPosArgs ) ;
        Py_DecRef( pAddFn ) ;
      }
    catch(string e){
        Output::instance().setLogError(e);
    }

}

InterpretePy::~InterpretePy()
{
    Py_Finalize() ;
}
