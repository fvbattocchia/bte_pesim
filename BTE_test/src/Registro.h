/*
 * Registro.h
 *
 *  Created on: 24/7/2017
 *      Author: Battocchia Florencia
 */

#ifndef REGISTRO_H_
#define REGISTRO_H_

#include <map>
using namespace std;


class Pin; //fordward declaration, para usar como puntero


class Registro {

public:
    Registro(bool, bool, int);
	bool getRead() {return read;}
	bool getWrite() {return write;}
	int getAddress() {return address;}
    void writeRegister(uint8_t);
    uint8_t readRegister();
    void crearPin(int, bool, int, Registro*, bool o, bool i);
    void crearBit(int,int);
    Pin* getPin(int);
    void setStateBit(int,int);
    int getStateBit(int);

	virtual ~Registro();
private:
	bool read;
	bool write;
	int address;
    map<int,int>bits;
    map<int,Pin*> pines;

};

#endif /* REGISTRO_H_ */
