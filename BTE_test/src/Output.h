#ifndef OUTPUT_H
#define OUTPUT_H
#include<QString>
#include <QFile>
#include <iostream>
using namespace std;
class QTextEdit;
class QFile;
class QMainWindow;
class Output

{
public:
    static Output &instance();
    void getEditor(QMainWindow *);
    void setLog(string);
    void logInfo(string);
    void kill(string);
    void setLogError(string);
    void setWarning(string);
    void setError(string);
    void saveLog(QString);
    void createFileLog(QString);
    QString remplazar(QString);


private:
    Output();
  //  static Output* out;
    QTextEdit *log;
    QTextEdit *sc;
    QString textoLog;
    QString textosc;
    QFile file;
    QMainWindow * obj;


};



#endif // Output
