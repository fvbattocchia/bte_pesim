#include "dialogtest.h"
#include "ui_dialogtest.h"

#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
DialogTest::DialogTest(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogTest)
{
    ui->setupUi(this);
    ui->tableTarea->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

DialogTest::~DialogTest()
{

    delete ui;
}

void DialogTest::on_guardar_clicked()
{
    emit saveTareas();
}

void DialogTest::on_agregarFila_clicked()
{
    QTableWidgetItem *item;
    int row;
    //si no esta nada seleccionado es -1
    if(ui->tableTarea->currentRow()>-1){
        row = ui->tableTarea->currentRow()+1;
    }
    else {
        row=ui->tableTarea->rowCount();
   }
   ui->tableTarea->insertRow(row);
   for(int i=0;i<6;i++){
        item = new QTableWidgetItem();
        ui->tableTarea->setItem(row, i, item);
   }
}

void DialogTest::on_eliminarFila_clicked()
{
    if(ui->tableTarea->currentRow()>-1)
        ui->tableTarea->removeRow(ui->tableTarea->currentRow());
    else
        ui->tableTarea->removeRow(ui->tableTarea->rowCount()-1);

}
