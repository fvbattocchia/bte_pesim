/*
 * Interfazcfcc.h
 *
 *  Created on: 28 ago. 2017
 *      Author: Battocchia Florencia
 */

#ifndef INTERFAZCFCC_H_
#define INTERFAZCFCC_H_
#include <list>
#include<stdint.h>
#include <string>
#include <iostream>
using namespace std;
class InterpretePy;
class ContenedorPlacas;
class Interfazcfcc {
public:
    Interfazcfcc(InterpretePy *);
	virtual ~Interfazcfcc();
    list<uint64_t> write(list<uint64_t>);
    void message(string);
private:
    ContenedorPlacas *boards;
    InterpretePy *py;
    list<string> names;

};

#endif /* INTERFAZCFCC_H_ */
