#ifndef INTERPRETEPY_H
#define INTERPRETEPY_H
#include <string>
#include <map>
#include <iostream>
#include <list>
using namespace std;
typedef struct _object PyObject;

class InterpretePy
{
public:
    InterpretePy();
    bool crearModulo(string,string);
    uint64_t write(string, string, uint64_t,bool,string);
    uint64_t read(string, string, string, bool);
    uint64_t write(string, string, uint64_t, uint64_t, bool, string);
    uint64_t read(string, string, uint64_t, string, bool);
    list<uint64_t>capaDeCom(string,list<uint64_t>);
    void ejecutarTarea(string, uint64_t, int);
    virtual ~InterpretePy();
private:
    map<string, PyObject*>modulos;


};

#endif // INTERPRETEPY_H
