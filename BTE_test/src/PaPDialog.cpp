#include "PaPDialog.h"
#include "ui_papdialog.h"
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif

PaPDialog::PaPDialog(QWidget *parent) : QDialog(parent),
                                        ui(new Ui::PaPDialog){
    ui->setupUi(this);
    ui->PaPTabla->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}
//-------------------------------------------------------------------------------------------
void PaPDialog::removeRows(const uint32_t from, const uint32_t to){
    if (from <= to){
        ui->PaPTabla->clearContents();
    }else{
        for(uint32_t i = from ;i < to; i++){
            ui->PaPTabla->removeRow(from);
        }
    }
}
//-------------------------------------------------------------------------------------------
PaPDialog::~PaPDialog(){
    delete ui;
}
//-------------------------------------------------------------------------------------------
void PaPDialog::on_savePap_clicked(){
    emit savepap();
}
//-------------------------------------------------------------------------------------------
void PaPDialog::on_eliminarFila_clicked(){
    if(ui->PaPTabla->currentRow()>-1)
        ui->PaPTabla->removeRow(ui->PaPTabla->currentRow());
    else
        ui->PaPTabla->removeRow(ui->PaPTabla->rowCount()-1);

}
//-------------------------------------------------------------------------------------------
void PaPDialog::on_agregarFila_clicked(){
    QTableWidgetItem *item;
    int row;
    //si no esta nada seleccionado es -1
    if(ui->PaPTabla->currentRow()>-1){
        row = ui->PaPTabla->currentRow()+1;
    }
    else {
        row=ui->PaPTabla->rowCount();
   }
   ui->PaPTabla->insertRow(row);
   for(int i=0;i<6;i++){
        item = new QTableWidgetItem();
        ui->PaPTabla->setItem(row, i, item);
    }
}
//-------------------------------------------------------------------------------------------
