#ifndef CORNERWIDGET_H
#define CORNERWIDGET_H

#include <QWidget>
#include <qtabwidget.h>

namespace Ui {
class CornerWidget;
}

class CornerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CornerWidget(QTabWidget *parent = 0);
    ~CornerWidget();

private slots:


    void on_agregarfila_clicked();

    void on_eliminarfila_clicked();

    void on_eliminartodo_clicked();

    void on_lupa_clicked();

    void on_lineEdit_returnPressed();

signals:
    void eliminartodo();
    void filaeliminada();




private:
    Ui::CornerWidget *ui;
    QTabWidget *tab;
};

#endif // CORNERWIDGET_H
