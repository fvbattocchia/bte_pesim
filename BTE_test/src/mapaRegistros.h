/*
 * mapaRegistros.h
 *
 *  Created on: 30 ago. 2017
 *      Author: Florencia
 */

#ifndef MAPAREGISTROS_H_
#define MAPAREGISTROS_H_
#include <iostream>
#include <functional>
#include <map>
#include <string>
#include <QObject>
#include <QTimer>
using namespace std;
class ConexionesPaP;
class InterpretePy;
class mapaRegistros:public QObject {
    Q_OBJECT
public:
    mapaRegistros(ConexionesPaP *, string, InterpretePy *);
    void crearRegName(string,string,string,string,string);
    void crearRegAddress(uint64_t,string,int,string,string,string,string);
    uint64_t count(uint64_t);
    uint64_t count(string);
	bool isWrite(string);
    bool isWrite(uint64_t);
	bool isRead(string);
    bool isRead(uint64_t);
    void write(string, uint64_t, bool);
    void write(uint64_t, uint64_t, bool);
    uint64_t read(string, bool);
    uint64_t read(uint64_t, bool);
	string info();
    void startClock(int);
    void stopClock();
    bool isClockOn(){return clockOn;}

    virtual ~mapaRegistros();
private slots:
    void clockTarea();

private:

    struct registroname {
        string access;
        string read;
        string write;
        string exit;

    };
    struct registroaddress {
            string name;
            int offset;
            string access;
            string read;
            string write;
            string exit;
    };
    map <uint64_t, registroaddress> registeraddress;
    map <string, registroname> registersname;
	registroname reg_n;
	registroaddress reg_a;
    ConexionesPaP *con;
    string placa;
    InterpretePy *py;
    QTimer timer;
    bool clockOn;
    uint64_t contador;
    int periodo;

};

#endif /* SRC_REGISTERINTERFACE_H_ */
