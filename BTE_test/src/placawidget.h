#ifndef PLACAWIDGET_H
#define PLACAWIDGET_H

#include <QWidget>

#include <Qsci/qsciscintilla.h>

namespace Ui {
class PlacaWidget;
}

class PlacaWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PlacaWidget(QWidget *parent = 0);
    void crearEditor();
    ~PlacaWidget();

private slots:


    void on_tabla_cellClicked(int row, int column);
    void on_tabla_cellChanged(int row, int column);
    void on_editorTex_textChanged();
signals:
    void cellChanged(PlacaWidget*,int,int);
    void modificationChanged();


private:
    Ui::PlacaWidget *ui;
    QsciScintilla *editor;
};

#endif // PLACAWIDGET_H
