#include "cornerwidget.h"
#include "ui_cornerwidget.h"
#include <QTableWidget>
#include <QtDebug>
#include <Qsci/qsciscintilla.h>
#include "Output.h"
#ifdef LEAK_CHECK
#include <cppMemDbg.h>
#endif
CornerWidget::CornerWidget(QTabWidget *parent) :
    QWidget(parent),
    ui(new Ui::CornerWidget)
{
    ui->setupUi(this);
    tab=parent;
    ui->lineEdit->hide();
}

CornerWidget::~CornerWidget()
{
    delete ui;
}

void CornerWidget::on_agregarfila_clicked()
{
    QTableWidget *tabla;
    QTableWidgetItem *item;
    int row;
    int index;
    index=tab->currentIndex();
    tabla=tab->widget(index)->findChild<QTableWidget *>("tabla");
    //si no esta nada seleccionado es -1
    if(tabla->currentRow()>-1) {
        row = tabla->currentRow()+1;
    }
    else {
        row=tabla->rowCount();
   }
   tabla->insertRow(row);
   for(int i=0;i<7;i++){
        item = new QTableWidgetItem();
        tabla->setItem(row, i, item);
    }

}

void CornerWidget::on_eliminarfila_clicked()
{
    QTableWidget *tabla;
    int index;
    index=tab->currentIndex();
    tabla=tab->widget(index)->findChild<QTableWidget *>("tabla");
    if(tabla->currentRow()>-1)
        tabla->removeRow(tabla->currentRow());
    else
        tabla->removeRow(tabla->rowCount()-1);
    emit filaeliminada();
}

void CornerWidget::on_eliminartodo_clicked()
{
    emit eliminartodo();
}

void CornerWidget::on_lupa_clicked()
{
    if(ui->lineEdit->isVisible())
        ui->lineEdit->hide();
    else
        ui->lineEdit->show();
}

void CornerWidget::on_lineEdit_returnPressed()
{
    QString comando=ui->lineEdit->text();
    QTableWidget *tabla;
    int index=tab->currentIndex();
    tabla=tab->widget(index)->findChild<QTableWidget *>("tabla");
    QsciScintilla *editor;
    editor=tab->widget(tab->currentIndex())->findChild<QsciScintilla *>("editorTex");
    if(!comando.isEmpty()){
        QRegularExpression re("([a-zA-Z]+[0-9a-zA-Z]*)");
        QRegularExpressionValidator v(re,0);
        int pos = 0;
        int state;
        state=v.validate(comando, pos);
        if(state==2){
            int fila=tabla->rowCount();
            for(int i=0;i<fila;i++){
               if(tabla->item(i,2)->text()==comando){
                    tabla->selectRow(i);
                    break;
                }
             }
        }
        else{
            Output::instance().setWarning("Nombre de registro incorrecto");
        }

   }
   else{
        tabla->clearSelection();
        int line_;
        int index_;
        editor->getCursorPosition(&line_,&index_);
        editor->setSelection(line_,0,line_,0);


       }

}
