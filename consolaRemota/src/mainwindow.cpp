#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QProcess>
#include <QTimer>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //QProcess::startDetached("C:/Program Files (x86)/mosquitto/mosquitto.exe");
    QProcess *mosquitto=new QProcess(this);

    mosquitto->setProgram("C:/workspaceQT/mosquitto/mosquitto.exe");
    mosquitto->start();

    client = new QMQTT::Client(QHostAddress::LocalHost, 1883);
    //error
    connect(client,SIGNAL(error(QMQTT::ClientError)),this,SLOT(error(QMQTT::ClientError)));
    //espera a que se conecte para subscribirse
    connect(client,SIGNAL(connected()),this,SLOT(connected()));
    //mensaje
    connect(client,SIGNAL(received(const QMQTT::Message&)),this,SLOT(received(const QMQTT::Message&)));
    conectar();
}

void MainWindow::setLog(QString log)
{
    //******************
    QTextCursor c = ui->textEdit->textCursor();
    c.movePosition(QTextCursor::End);
    c.insertHtml(QString("<span style=\"color: %2\">%1</span><br>")
                         .arg(log)
                         .arg("rgb(23,21,21)"));
    if(c.position()>10000){
       ui->textEdit->clear();
    }

    ui->textEdit->setTextCursor(c);
    //*********************

 //   QTextCursor c = ui->textEdit->textCursor();
 //   c.movePosition(QTextCursor::End);
 //   ui->textEdit->insertPlainText(log+"\n");
 //   ui->textEdit->setTextCursor(c);
    qDebug()<<log;
}

MainWindow::~MainWindow()
{
    client->disconnected();
    delete ui;
}
void MainWindow::connected()
{
     client->subscribe("log",0);
     //client->subscribe("placa",0);
}

void MainWindow::received(const QMQTT::Message& message)
{
    setLog(QString(message.payload()));
}

void MainWindow::error(QMQTT::ClientError e)
{

    switch (e) {
        case QMQTT::ClientError::UnknownError:
            setLog("UnknownError");
            break;
        case  QMQTT::ClientError::SocketConnectionRefusedError:
            setLog("SocketConnectionRefusedError");
            break;
        case QMQTT::ClientError::SocketRemoteHostClosedError:
            setLog("SocketRemoteHostClosedError");
            break;
        case QMQTT::ClientError::SocketHostNotFoundError:
            setLog("SocketHostNotFoundError");
            break;
        case QMQTT::ClientError::SocketAccessError:
            setLog("SocketAccessError");
            break;
        case QMQTT::ClientError::SocketResourceError:
            setLog("SocketResourceError");
            break;
        case QMQTT::ClientError::SocketTimeoutError:
            setLog("SocketTimeoutError");
            break;
        case QMQTT::ClientError::SocketNetworkError:
            setLog("SocketNetworkError");
            break;
        case QMQTT::ClientError::SocketAddressInUseError:
            setLog("SocketAddressInUseError");
            break;
        case QMQTT::ClientError::SocketAddressNotAvailableError:
            setLog("SocketAddressNotAvailableError");
            break;
        case QMQTT::ClientError::SocketUnsupportedSocketOperationError:
            setLog("SocketUnsupportedSocketOperationError");
            break;
        case QMQTT::ClientError::SocketUnfinishedSocketOperationError:
            setLog("SocketUnfinishedSocketOperationError");
            break;
        case QMQTT::ClientError::SocketProxyAuthenticationRequiredError:
            setLog("SocketProxyAuthenticationRequiredError");
            break;
        case QMQTT::ClientError::SocketSslHandshakeFailedError:
            setLog("SocketSslHandshakeFailedError");
            break;
        case QMQTT::ClientError::SocketProxyConnectionRefusedError:
            setLog("SocketProxyConnectionRefusedError");
            break;
        case QMQTT::ClientError::SocketProxyConnectionClosedError:
            setLog("SocketProxyConnectionClosedError");
            break;
        case QMQTT::ClientError::SocketProxyConnectionTimeoutError:
            setLog("SocketProxyConnectionTimeoutError");
            break;
        case QMQTT::ClientError::SocketProxyNotFoundError:
            setLog("SocketProxyNotFoundError");
            break;
        case QMQTT::ClientError::SocketOperationError:
            setLog("SocketOperationError");
            break;
        case QMQTT::ClientError::SocketSslInternalError:
            setLog("SocketSslInternalError");
            break;
        case QMQTT::ClientError::SocketSslInvalidUserDataError:
            setLog("SocketSslInvalidUserDataError");
            break;
       default:
            break;
    }
}
void MainWindow::on_consola_returnPressed()
{
    qDebug()<<"entro";
    QString comando=ui->consola->text();
    if(!comando.isEmpty())
        client->publish(QMQTT::Message(0,"comando",comando.toLocal8Bit()));
    ui->consola->clear();
}

void MainWindow::conectar()
{
    if(!client->isConnectedToHost()){
        //lo conecta
        client->connectToHost();
        QTimer::singleShot(1000, this, SLOT(conectar()));
    }
}
