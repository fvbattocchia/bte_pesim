#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qmqtt.h"
#include <QString>
#include "QProcess"
namespace Ui {

class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void setLog(QString);
    ~MainWindow();

private slots:
    void connected();
    void received(const QMQTT::Message&);
    void error(QMQTT::ClientError);
    void on_consola_returnPressed();
    void conectar();
private:
    Ui::MainWindow *ui;
    QMQTT::Client *client;
};

#endif // MAINWINDOW_H
