#-------------------------------------------------
#
# Project created by QtCreator 2017-10-08T17:04:07
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = consolaRemota
TEMPLATE = app
#qtmqtt
win32:INCLUDEPATH += ../../Librerias/Qtmqtt\include
LIBS += -L ..\\..\\Librerias\\Qtmqtt\\lib -lQtmqtt

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
