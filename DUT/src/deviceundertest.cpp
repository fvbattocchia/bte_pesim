#include "deviceundertest.h"
#include "ui_deviceundertest.h"
#include <QTimer>
#include <QDebug>
#include<inpout32.h>
#include <iostream>
#include <QApplication>
using namespace std;

#define WRITEn 		0xfe
#define ADDRSTRBn 	0xf7
#define DATASTRBn 	0xfd
#define WAIT		0x80
#define DIRECTION 	0xDF
/*
mapa de bits
placa 2 lectura
mv1(66) mv2(67) ev1 ev2 pb ph gen
placa 1 escritura
ev1 eve2 gen

*/
deviceUnderTest::deviceUnderTest(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::deviceUnderTest)
{
    ui->setupUi(this);
    basePort=0x378;
    portAddress = basePort;
    Control_Register = 2;
    Address_Register = 3;
    Data_Register = 4;
    sd=0x0;

    timer=new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(leerRegistros()));
}

deviceUnderTest::~deviceUnderTest()
{
    delete ui;
}

void deviceUnderTest::on_abrirEV1_clicked()
{
    int registerAddress;
    uint8_t registerData;
    if(IsInpOutDriverOpen()){
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));

        registerAddress=Address_Register;
        registerData = 0x1;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x2;
        Out32(portAddress + registerAddress, registerData);

        //prendo el bit 0
        registerAddress=Data_Register;
        registerData=sd|0x1;
        sd=registerData;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x7;
        Out32(portAddress + registerAddress, registerData);
    }
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }
}

void deviceUnderTest::on_cerrarEV1_clicked()
{
    int registerAddress;
    uint8_t registerData;
    if(IsInpOutDriverOpen()){
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));

        registerAddress=Address_Register;
        registerData = 0x1;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x2;
        Out32(portAddress + registerAddress, registerData);

        //apago el bit 0
        registerAddress=Data_Register;
        registerData=sd&WRITEn;
        sd=registerData;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x7;
        Out32(portAddress + registerAddress, registerData);
    }
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }
}

void deviceUnderTest::on_abrirEV2_clicked()
{
    int registerAddress;
    uint8_t registerData;
    if(IsInpOutDriverOpen()){
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));

        registerAddress=Address_Register;
        registerData = 0x1;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x2;
        Out32(portAddress + registerAddress, registerData);

        //prendo el bit 1
        registerAddress=Data_Register;
        registerData=sd|0x2;
        sd=registerData;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x7;
        Out32(portAddress + registerAddress, registerData);
    }
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }
}

void deviceUnderTest::on_cerrarEV2_clicked()
{
    int registerAddress;
    uint8_t registerData;
    if(IsInpOutDriverOpen()){
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));

        registerAddress=Address_Register;
        registerData = 0x1;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x2;
        Out32(portAddress + registerAddress, registerData);

        //apago el bit 1
        registerAddress=Data_Register;
        registerData=sd&DATASTRBn;
        sd=registerData;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x7;
        Out32(portAddress + registerAddress, registerData);
    }
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }
}

void deviceUnderTest::on_prenderGen_clicked()
{
    int registerAddress;
    uint8_t registerData;
    if(IsInpOutDriverOpen()){
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));

        registerAddress=Address_Register;
        registerData = 0x1;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x2;
        Out32(portAddress + registerAddress, registerData);

        //prendo el bit 2
        registerAddress=Data_Register;
        registerData=sd|0x4;
        sd=registerData;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x7;
        Out32(portAddress + registerAddress, registerData);
    }
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }
}

void deviceUnderTest::on_apagarGen_clicked()
{
    int registerAddress;
    uint8_t registerData;
    if(IsInpOutDriverOpen()){
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));

        registerAddress=Address_Register;
        registerData = 0x1;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x2;
        Out32(portAddress + registerAddress, registerData);

        //apago el bit 2
        registerAddress=Data_Register;
        registerData=sd&0xfb;
        sd= registerData;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x7;
        Out32(portAddress + registerAddress, registerData);
    }
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }
}

void deviceUnderTest::on_prenserSis_clicked()
{

    ui->apagarSis->setStyleSheet("background-image: url(:/gris-ceniza.png);");
    ui->prenserSis->setStyleSheet("background-image: url(:/verde.png);");
    if(IsInpOutDriverOpen())
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }
    timer->start(500);
}

void deviceUnderTest::on_apagarSis_clicked()
{
    ui->apagarSis->setStyleSheet("background-image: url(:/rojo.jpg);");
    ui->prenserSis->setStyleSheet("background-image: url(:/gris-ceniza.png);");

    int registerAddress;
    uint8_t registerData;
    if(IsInpOutDriverOpen()){
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));

        registerAddress=Address_Register;
        registerData = 0x1;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x2;
        Out32(portAddress + registerAddress, registerData);

        //apaga generador y EV
        registerAddress=Data_Register;
        registerData = 0x0;
        sd=registerData;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x7;
        Out32(portAddress + registerAddress, registerData);
    }
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }
    timer->stop();

}

void deviceUnderTest::leerRegistros()
{
    int registerAddress;
    uint8_t registerData;
    //LEE ENTRADAS DIGITALES
    if(IsInpOutDriverOpen()){
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));

        registerAddress=Address_Register;
        registerData = 0x2;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x1;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 36;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        uint8_t readValue;
        readValue = Inp32(portAddress + registerAddress);
       // cout<<"ffem/aimi: "<<hex<<int(readValue)<<endl;

        registerAddress=Data_Register;
        registerData = 0x7;
        Out32(portAddress + registerAddress, registerData);

        int mv1=readValue&0x1;
        int mv2=(readValue>>1)&0x1;
        int ev1=(readValue>>2)&0x1;
        int ev2=(readValue>>3)&0x1;
        int pb=(readValue>>4)&0x1;
        int ph=(readValue>>5)&0x1;
        int gen=(readValue>>6)&0x1;

        if(mv1==1)
            ui->estadoVM1->setIcon(QIcon(":/led_verde.png"));
        else
            ui->estadoVM1->setIcon(QIcon(":/led_gris.png"));
        if(mv2==1)
            ui->estadoVM2->setIcon(QIcon(":/led_verde.png"));
        else
            ui->estadoVM2->setIcon(QIcon(":/led_gris.png"));
        if(ev1==1){
            ui->estadoEV1->setIcon(QIcon(":/led_verde.png"));
            escsalidaAnalogic(255);
        }
        else{
            ui->estadoEV1->setIcon(QIcon(":/led_gris.png"));
            escsalidaAnalogic(0);
        }
        if(ev2==1)
            ui->estadoEV2->setIcon(QIcon(":/led_verde.png"));
        else
            ui->estadoEV2->setIcon(QIcon(":/led_gris.png"));
        if(gen==1)
            ui->estadoGen->setIcon(QIcon(":/led_verde.png"));
        else
            ui->estadoGen->setIcon(QIcon(":/led_gris.png"));
        if(pb==1)
            ui->estadoM->setIcon(QIcon(":/led_rojo.png"));
        else
            ui->estadoM->setIcon(QIcon(":/led_verde.png"));
        if(ph==1)
            ui->estadoM->setIcon(QIcon(":/led_rojo.png"));
        else
            ui->estadoM->setIcon(QIcon(":/led_verde.png"));
        QApplication::instance()->processEvents();
    }
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }

    int ValueL=0;
    int ValueH=0;
    //LEE ENTRADAS ANALOGICAS
    if(IsInpOutDriverOpen()){
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));

        registerAddress=Address_Register;
        registerData = 0x2;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x1;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 40;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        ValueL = Inp32(portAddress + registerAddress);
     //   cout<<"ffem/aimi: "<<hex<<int(ValueL)<<endl;

        registerAddress=Data_Register;
        registerData = 0x7;
        Out32(portAddress + registerAddress, registerData);
    }
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }
    if(IsInpOutDriverOpen()){
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));

        registerAddress=Address_Register;
        registerData = 0x2;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x1;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 41;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        ValueH = Inp32(portAddress + registerAddress);
       // cout<<"ffem/aimi: "<<hex<<int(ValueH)<<endl;

        registerAddress=Data_Register;
        registerData = 0x7;
        Out32(portAddress + registerAddress, registerData);
    }
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }
    int valor=(ValueH<<8)|ValueL;
    QString str;
    str.setNum(valor);
    ui->sensorQ->setText(str);
    QApplication::instance()->processEvents();

    //***************************************************+
    if(IsInpOutDriverOpen()){
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));

        registerAddress=Address_Register;
        registerData = 0x2;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x1;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 38;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        ValueL = Inp32(portAddress + registerAddress);
       // cout<<"ffem/aimi: "<<hex<<int(ValueL)<<endl;

        registerAddress=Data_Register;
        registerData = 0x7;
        Out32(portAddress + registerAddress, registerData);
    }
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }
    if(IsInpOutDriverOpen()){
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));

        registerAddress=Address_Register;
        registerData = 0x2;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x1;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 39;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        ValueH = Inp32(portAddress + registerAddress);
        //cout<<"ffem/aimi: "<<hex<<int(ValueH)<<endl;

        registerAddress=Data_Register;
        registerData = 0x7;
        Out32(portAddress + registerAddress, registerData);
    }
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }
    valor=(ValueH<<8)|ValueL;
    str.setNum(valor);
    ui->sensorX->setText(str);

}

void deviceUnderTest::escsalidaAnalogic(int dato)
{
    int registerAddress;
    uint8_t registerData;
    if(IsInpOutDriverOpen()){
        ui->estadoCom->setIcon(QIcon(":/led_verde.png"));

        registerAddress=Address_Register;
        registerData = 0x1;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x4;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = dato;
        Out32(portAddress + registerAddress, registerData);

        registerAddress=Data_Register;
        registerData = 0x7;
        Out32(portAddress + registerAddress, registerData);
    }
    else{
        ui->estadoCom->setIcon(QIcon(":/led_rojo.png"));
        cout << "port close" << endl;
    }

}

void deviceUnderTest::on_pushButton_clicked()
{
    leerRegistros();
}
