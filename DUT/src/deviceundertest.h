#ifndef DEVICEUNDERTEST_H
#define DEVICEUNDERTEST_H

#include <QMainWindow>
#include <windows.h>

namespace Ui {
class deviceUnderTest;
}

class deviceUnderTest : public QMainWindow
{
    Q_OBJECT

public:
    explicit deviceUnderTest(QWidget *parent = 0);
    ~deviceUnderTest();

private slots:
    void on_abrirEV1_clicked();

    void on_cerrarEV1_clicked();

    void on_abrirEV2_clicked();

    void on_cerrarEV2_clicked();

    void on_apagarGen_clicked();

    void on_prenderGen_clicked();

    void on_prenserSis_clicked();

    void on_apagarSis_clicked();

    void leerRegistros();

    void escsalidaAnalogic(int );

    void on_pushButton_clicked();

private:
    Ui::deviceUnderTest *ui;
    __int8 sd;
    WORD basePort;
    WORD portAddress;
    int Control_Register;
    int Address_Register;
    int Data_Register;
    QTimer *timer;



};

#endif // DEVICEUNDERTEST_H
