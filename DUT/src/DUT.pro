#-------------------------------------------------
#
# Project created by QtCreator 2017-10-09T14:59:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DUT
TEMPLATE = app


INCLUDEPATH += ../../Librerias/inpout32 \
               ../../Librerias/nanomsg\include

#soporte de pthread
QMAKE_CXXFLAGS += -pthread


SOURCES += main.cpp\
        deviceundertest.cpp

HEADERS  += deviceundertest.h

FORMS    += deviceundertest.ui

RESOURCES += \
    imgs/img.qrc

LIBS += -L..\\..\\Librerias\\inpout32\\Release -linpout32
#nanomsg
LIBS += -L..\\..\\Librerias\\nanomsg\\lib -lnanomsg
LIBS += -pthread
